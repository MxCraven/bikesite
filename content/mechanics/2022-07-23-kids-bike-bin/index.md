+++
title = "The Kids Bike From The Bin"
date = 2022-07-23T20:15:00
[taxonomies]
tags = ["audio", "mechanics", "kids-on-bikes"]
+++

{{ audio(path="untitled.mp3") }}

![](IMG_20220705_161026.jpg "A small kids bike strapped onto my commuter bike rear rack, badly")
![](IMG_20220705_161030.jpg "A close shot of the bike, you can see it's tangled in bungee cord")
![](IMG_20220705_162106.jpg "Handlebars removed from the bike to assess the headset")
![](IMG_20220705_165756.jpg "Handlebars repaired and looking at the steering and wheel")
![](IMG_20220706_094041.jpg "Cranks off, seeing there's only bushings no bearings")
![](IMG_20220706_101521.jpg "Front view of the bike, it's green and black and small")
![](IMG_20220706_101532.jpg "Close up of the rear wheel")
![](IMG_20220706_101541.jpg "Close up of the front wheel")
![](IMG_20220706_101632.jpg "A view over the top of the bike")
![](IMG_20220706_101513.jpg "A side on view of the bike, with green pedals, half green saddle, some BEN10 decals.")

