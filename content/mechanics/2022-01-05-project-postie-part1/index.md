+++
title = "Project Postie Part 1 | The Before Project"
date = 2022-01-05T17:51:10Z
[taxonomies]
tags = ["project-postie", "mechanics", "audio"]
+++

{{ audio(path="part1.mp3") }}
Part 1 of a journey of building bikes. Use the tags to see all posts!

[All Project Postie Things](/tags/project-postie)

![](IMG_20210411_144323.jpg "The bike as it originally came from my Grandad. It's a red framed bike with white mudguards and chainguard. The front has a blue basket on a black rack. The bike has messy cables and is very dirty. The seat is old and worn.")

![](IMG_20210414_120150.jpg "The same bike, but now cleaner, with the random tape removed and dirt cleaned as best possible. The saddle has been replaced with a more comfortable modern one")

![](IMG_20210414_120154.jpg "Same as previous picture, but with a different angle")

![](IMG_20210426_124713.jpg "The pedals have been changed to new light blue DMR V7s, the grips are now foam ESI in the same light blue.")

![](IMG_20210505_102505.jpg "The gear shifter that was on the top tube has been moved to the handlebars, and the cable is fresh. The bike is lent against a different wall from previous pictures.")

![](IMG_20210505_102508.jpg "The same as previous, but a different angle")

![](IMG_20210505_102519.jpg "The same as previous but from a front on angle. The bike is very thin")

![](IMG_20210506_180151.jpg "The bike now has a silver horn on the bars, mounted sideways")

![](IMG_20210506_180157.jpg "The same picture, but as viewed from someone sat on the bike.")

![](IMG_20210519_143014.jpg "The blue basket has been replaced with a wicker picnic basket.")

![](IMG_20210519_143024.jpg "A front on view of the basket") 

![](IMG_20210519_143051.jpg "A front on, but further back view") 

