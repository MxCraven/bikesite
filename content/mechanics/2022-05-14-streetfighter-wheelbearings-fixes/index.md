+++
title = "Making Bikes Look Good on the Cheap, and Breaking Shit" 
date = 2022-05-15T18:00:00
[taxonomies]
tags = ["mechanics", "audio", "ramble", "aesthetic"]
+++

{{ audio(path="audio.mp3") }}

![](IMG_20220514_174419.jpg "The back end of my Scott Speedster CX bike showing the rear mech with cable disconnected and tooth marks from a pair of plyers in the cable housing and ferrule")

![](IMG_20220514_175337.jpg "The inline barrel adjuster for the rear gear cable has teeth marks from plyers")

![](IMG_20220509_163650.jpg "My streetfighter commuter with a large box on top of the rack held down by a cargo net")

![](IMG_20220514_164234.jpg "The commuter before upgrades. It has a bag over the seat, upwards stem with chimney, and cable tie ends hanging everywhere")

![](IMG_20220514_171236.jpg "The sleeked up look for the commuter, with tidied cable ends and a stacked up but flipped to point down stem which looks much more sick. The saddle cover has gone too, revealing a black seat")

![](IMG_20220514_180651.jpg "Two QR skewers. One is rusty as fuck and has no springs, the other is a tiny bit dirty but has both springs")





