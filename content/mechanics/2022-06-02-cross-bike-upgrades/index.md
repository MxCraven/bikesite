+++
title = "Long Overdue Upgrades/Repairs to my Crosser"
date = 2022-06-02T12:00:00
[taxonomies]
tags = ["text", "mechanics"]
+++ 
This post is gonna be a mess. The stuff I did happened 2 weeks ago now, and this was meant to be a speaky one but *shit gone down* so I'm sat on my Grandparents sofa, writing this up, while watching stuff about some old lady who wrote Bohemian Rhapsody :P

Anyway, my wheel broke, and I knew it was dying, but I did nothing about it for fear of making it worse right before a big ride, and then it completely locked up on me, probably just to spite me. 

![](IMG_20220519_162829.jpg "A rear bike wheel with a chainwhip and lockring remover in the cassette")
Whip the locknut off, and pull out the cassette
![](IMG_20220519_164006.jpg "Some ball bearings in a magnetic bowl. There's 2 piles")
Took out the bearings, and one side was horrible. Of course it was the drive side! 
![](IMG_20220519_164012.jpg "Cleaning out the bearing race on the rear wheel drive side")
The races were perfect still, so that was good. But the quality of the freehub was awful, so I tried to remove it but... Apparently 12mm is required, and I don't have one of those. I always thought it was 10mm anyway. 
![](IMG_20220519_164020.jpg "Trying a 10mm hex to remove the freehub")
![](IMG_20220519_164434.jpg "A clean bearing race on the other side")

In between these pictures, some forgot to be taken, and many were lost to some corruption for no reason. I forgot to take pitcures of all the headset stuff, but that went uhh... As smoothly as it could I guess? But the bearings first. Cleaned them all up, threw away the bearings because they were quire trashed, and replaced them with fresh balls and fresh grease. It's great running now. A tiny wobble I need to sort out, and at some point I need to replace/get the freehub serviced. I don't think I'll be doing that myself though as it's very difficult, involved, and requires many tools I'll never use again. 

As for the headset, removing the crown race was a fight. Eventually, getting it slightly up with a weird business card multitool my Dad had, then jamming various degrees of spanners into it with a hammer, and prying upwards worked after a good 20 mins of trying.   
The reinstall was very easy. Pop out the cups with 2-3 hits (really don't see the need for a cup remover if you've got a long hex like thing, or a screwdriver). Putting the cups in went perfectly with a bit of wood and a few whacks gently. 

The new crown race was split, as the new headset is cartridge style (amazing). I made sure it was set by overtightening the whole system to squish it down. There's a small gap, but there always was before anyway. It just got covered by the plastic bearing cover that was needed before. 

The silver headset looks really nice against the silver details on the front of the bike, and (as seen later) I put a silver bar plug in, which I think is the small details I needed.

![](IMG_20220520_172447.jpg "my cut up finger")
Had a few accidents that day. One (the plaster on thumb) at work, and the rest were from the chainring as I tried internal routing. The internal routing tool is stuck in my frame because it fell off... I'll try to remember to remove it when I next whip the BB out and can get it. Eventually it did go through. 
![](IMG_20220520_174247.jpg "The one side of my bike with new bar tape and a silver bar end plug")
![](IMG_20220520_175519.jpg "My bike, looking the same as it did before, but feeling a million times better")

The bike looks no different, but the new gear cable feels amazing and somehow hasn't stretched yet which is good. When I first put the bar tape on one side, and had the old on the other you could really see the difference between brand new and over a year old, but then once I'd changed both you can't see anything, and I've ridden 100ish miles on it now so it's probably just as dirty. 

All in, I'm glad I changed the cables, and the headset is so so so nice. Maybe I'll actually change cables on some of my other bikes. Ive got some new brakes coming for my commuter, because holy shit the cheap ones were bad and I regret getting them. On a commuter they're powerful enough, but a year of abuse and the bolt holding the cable has stripped when I went to replace the pads :/ 

I've got some Tektro Novelas coming. Not incredible, but they'll be reliable. I did consider trying to get the SRAM Juicy 3s the bike used to have serviced and set up, but I think I'll try to sell those, and get a set of mineral oil ones for my MTB at some point in the future. As a bonus, the bike will look nicer due to slimmer brake levers. 

Anyway, thanks for reading, thanks for allowing me to ramble on the Internet, and uhh... Yeah. Have a great time. Wash ya bike. Overoil the chain, and have a great ride <3 