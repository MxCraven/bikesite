+++
title = "3, 2, 1, BEEP!"
date = 2021-12-30T13:02:03Z
[taxonomies]
tags = ["text"]
+++

{{ audio(path="321beep.mp3") }}

Here's the first post on this here webbed site I'm creating.   
  
On this site I'm going to be talking about bikes a lot. It's my favourite thing in the world. I was going to make it a video series, but video editing is hard, and taking pictures of what I do, then narrating over them for you seems a reasonable option. 

I hope you can all enjoy what I'm going to post here!
