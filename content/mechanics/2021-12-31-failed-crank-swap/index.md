+++
title = "Failed Crank Swap"
date = 2021-12-31T16:32:13Z
[taxonomies]
tags = ["mechanics", "audio"]
+++

{{ audio(path="failed-crank-swap.mp3") }}

[Part 2 Here](/mechanics/success-crank-swap)

Fig. 01
![The lower half of a mountain bike. It has yellow tyres, orange pedals, and a basic one by crank](IMG_20211231_093611.jpg "The lower half of a mountain bike. It has yellow tyres, orange pedals, and a basic one by crank")
Fig. 02 
![A view from the top of the bike, down towards the cranks and BB. A sticker on the top tube is visible. It has a sealed square taper BB.](IMG_20211231_093615.jpg "A view from the top of the bike, down towards the cranks and BB. A sticker on the top tube is visible. It has a sealed square taper BB.")
Fig. 03
![The bike with removed drive side crank](IMG_20211231_094342.jpg "The bike with removed drive side crank")
Fig. 04
![Both cranks on the floor, one with the crank removal tool still in it. A pedal spanner lays next to them. The BB is visible with no cranks on it, still installed in the frame.](IMG_20211231_094552.jpg "Both cranks on the floor, one with the crank removal tool still in it. A pedal spanner lays next to them. The BB is visible with no cranks on it, still installed in the frame.")
Fig. 05
![The removed sealed BB sits next to the potential new cranks on the floor](IMG_20211231_095040.jpg "The removed sealed BB sits next to the potential new cranks on the floor")
Fig. 06
![The frame with a rag through it to clean out the BB shell](IMG_20211231_095043.jpg "The frame with a rag through it to clean out the BB shell")
Fig. 07
![The whole bike is now in a stand on the driveway. No cranks or bottom bracket is installed](IMG_20211231_095440.jpg "The whole bike is now in a stand on the driveway. No cranks or bottom bracket is installed")
Fig. 08
![The new holotech bottom bracket is in the bag and on the workstand table. It's silver with a black internal](IMG_20211231_095445.jpg "The new holotech bottom bracket is in the bag and on the workstand table. It's silver with a black internal")
Fig. 09
![Starting to install the bottom bracket. The drive side is half installed.](IMG_20211231_095855.jpg "Starting to install the bottom bracket. The drive side is half installed.")
Fig. 10
![From the non-driveside you can see through the bottom bracket. The middle sleeve is installed, and the threads are greased.](IMG_20211231_100250.jpg "From the non-driveside you can see through the bottom bracket. The middle sleeve is installed, and the threads are greased.")
Fig. 11
![Both sides are now installed, and the cranks are test fitted. They do fit!](IMG_20211231_100527.jpg "Both sides are now installed, and the cranks are test fitted. They do fit!")
Fig. 12
![A view of all the cranks, some tools, cleaning materials, and grease.](IMG_20211231_100731.jpg "A view of all the cranks, some tools, cleaning materials, and grease.")
Fig. 13
![The new cranks are quite dirty with the chainrings removed](IMG_20211231_101404.jpg "The new cranks are quite dirty with the chainrings removed")
Fig. 14
![The chainring from the old cranks fitted to the new ones showing that it doesn't fit.](IMG_20211231_102504.jpg "The chainring from the old cranks fitted to the new ones showing that it doesn't fit.")
