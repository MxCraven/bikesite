 +++
title = "A seized wheel, and a tale of cleaning"

[taxonomies]
tags = ["text", "mechanics", "clean-your-bike"]
+++

To the guy that owns this wheel, if you can't be arsed to read my ramblings my recommendations are clean your bike more, the grease was replaced with mud, and that if the wheel wobbles let me know I've probably not got the screws set quite right. 

A friend of my Dads brought over his rear wheel a little while ago as it had seized up in the freehub. It was really bad. After I got it free it was spinning both ways. Clearly clogged with mud. First step? Spray WD40 into it, see if it clears. 

It did clear, but I wanted more, so I removed the bearings and decided I was gonna take the hub off entirely. Removing the cassette had shown a thick layer of mud inside all the cracks that are usually sealed (and worryingly, a very tight lockring on a loose cassette, but we get back to that later)

![](IMG_20221112_134500.jpg "A metal bowl with some very dirty ball bearings in, and a QR axle.")![](IMG_20221112_134502.jpg "Zoomed in on the cone nut to show how dirty it is, and how the grease has been replaced with mud")

As you can see it's very dirty!

![](IMG_20221112_134514.jpg "The inside of the hub, slightly rusty on the bearing surfaces")

The insides never really did get clean, but don't get it right just get it running. The hub is probably close to trashed, but I know this is a commuter bike so I'm happy with this effort. 

You can see how much shit is coming out. This is the 3rd going over with WD40. It must have been minging in there.

![](IMG_20221208_124718.jpg "A cassette freewheel with a lot of dirty WD40 water coming out")

I thought that a freewheel was a 12mm allen, but having aquired one it's not, it's bloody 11... So I had to just say fuck it and flush the thing out with more WD40. It seems to have worked, and I've got some chain oil thrown down the back of it for good measure (it's worn enough to give a gap for that to get in, which is also what let the mud in). I dread to think what's actually in there after all this... 

![](IMG_20221208_124952.jpg "A pot of rusted but clean bearings")

The bearings were shot, but I hadn't got a full set of new ones on hand, so back in they go! They'll be good enough, and the roughness of the hub shell and the cones will likely wreck the bearings anyway. I did put the better ones into the drive side (the side that was worse before) though, so that might help a bit. 

![](IMG_20221208_125754.jpg "A hub with the axle back in, all greased up")

Next up, clean the cassette. It was -1C when I did this, and I had to break the ice in a bucket to use it... But hey, I love this shit! :3 

![](IMG_20221208_130435.jpg "A dirty bike cassette on a zip tie")

This is what was left. Metal shavings from cleaning the whole cassette... Absolutely grim what must have been on that chain. The chain is definitely worn, I can tell from the cogs. They should be alright though. Hell, only 2 cogs were really worn (as happens often with commute bikes)

![](IMG_20221208_131618.jpg "A metal magnetic tray with 2 black rings of metal shaving gunk on")

Shiny!

![](IMG_20221208_131619.jpg "A cleaned cassette laying in parts on a slab")

I mentioned earlier the wobble. This cassette was missing a spacer somewhere. No idea where (probably the back) but it didn't have enough, allowing the lockring to be overtightened (bottoming out the threads) and the cassette to still wobble a lot. I found a random old spacer and jammed it on. It's not perfect (it's not correct at all even) but damn it it works. I should probably get a stack of a few spacers for this sort of thing. It's rare to see, but it'd be nice to have some spare (and somehow I've not got any, probably because in the past cassette replacing has always been a shop job and I've not worn out a cassette in the past 3 years I've been doing my own deep dive mechanics)

![](IMG_20221208_132622.jpg "A freehub with a spacer on it")

![](IMG_20221208_133549.jpg "The wheel all back together, with the cassette clean and on the wheel")

I hope the tightness of the cup and cones is okay. It's hard to test without putting it in a bike, but it should be good enough I hope. It runs stiff, but smooth which is what I aimed for (although if it does need tightening, which it might having compared to my own road bike wheel) then it's gonna run a bit rougher. 

I've actually given the fuck up on the advice of "A tiny bit of play is good for QR style wheels" because damn it just makes it so hard to find the right place, because different tightness of QR skewers fucks it, so I'm going for the other way that we do all other bearings. As loose as possible with zero play.   
  
My rear wheel on my own road bike has gone really gritty recently (probably after my [escapdes into the dirt](/rides/cx-mudder) TBH) so as with last time I had to rebuild a wheel, I end up doing 2-3 at the same time... Ah well, that life time supply of grease is getting a bit low, might need to top that up! (life time supplies of bike things always seem to run low when you get deep into bike mechanics. I've lived my life on probably 2 tubes of grease before now, but I can go through a tube in one wheel! How much grease? Lots!)

Anyway, there we go. Cya next time