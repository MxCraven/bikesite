+++
title = "A Gentle Reminder To Adjust Your Brakes"
date = 2022-01-02T12:15:13Z
[taxonomies]
tags = ["mechanics", "audio"]
+++

{{ audio(path="brake-pads.mp3") }}
Why don't my brakes work?! Well, at least I'm going downhill, right? :P

![](IMG_20220102_104620.jpg "A boxed pair of Disc Brake Pads from DiscoBrakes. Blue package with black pads inside")

![](IMG_20220102_092838.jpg "A front view of my front brake with the wheel installed")

![](IMG_20220102_093121.jpg "The cotterpin removed from the brake")

![](IMG_20220102_093339.jpg "The brake pads are removed and sat on the stand bench. The pads have roughly 2.5mm of pad left")

![](IMG_20220102_093537.jpg "Another set of pads on the stand bench. Again with 2.5mm of pad left. This is the rear set")

![](IMG_20220102_094450.jpg "The brake with the cable partially removed. The locknuts have been aligned to allow the cable to escape")

![](IMG_20220102_095131.jpg "Putting WD40 with PTFE down the brake cable")

![](IMG_20220102_095500.jpg "The cable head installed into the brake lever")

![](IMG_20220102_100110.jpg "Rubbing a brake pad against a brick in a wall. The wall is black where the dirt off the pad has come off")

![](IMG_20220102_102029.jpg "An adjustable spanner/monkey wrench around my brake disc. Used for straightening")

![](IMG_20220102_104049.jpg "The front brake all reinstalled and a new cap added (silver this time)")

![](IMG_20220102_104054.jpg "The rear brake all installed and adjusted")

