+++
title = "Brake Cable Rub"
date = 2021-12-31T17:22:56Z
[taxonomies]
tags = ["mechanics", "audio"]
+++

{{ audio(path="brake-cable-rub.mp3") }}

Fig. 01
![The front end of a slightly muddy bike. The cables run a mess through the front and down the bike.](IMG_20211231_110735.jpg "The front end of a slightly muddy bike. The cables run a mess through the front and down the bike.")

Fig. 02
![A close up of the top of the fork. You can see where the paint has been rubbed off by the brake cable](IMG_20211231_110738.jpg  "A close up of the top of the fork. You can see where the paint has been rubbed off by the brake cable")

Fig. 03 
![The brake cable has been rerouted to not destroy the paint, and the front end looks much neater](IMG_20211231_111019.jpg "The brake cable has been rerouted to not destroy the paint, and the front end looks much neater")
