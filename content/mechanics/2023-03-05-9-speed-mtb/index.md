 +++
title = "MTB Upgrades, Fork and Gears!"

[taxonomies]
tags = ["text", "mechanics", "mtb", "chain-wear"]
+++

So, it's been a while, I've done things I should have written about, but here goes something that's happened over the last few weeks. I upgraded my MTB to 9 speed! This was due to having a 9 speed shifter I was gonna use on the GT (post up at the same time as this one) but I wanted more gears on this bike, so I did it. There were issues, because of fucking course there were. 

![](IMG_20230214_164443.jpg "The back of an MTB shifter. It's a Shimano SL-M580")

![](IMG_20230217_161309.jpg "My MTB in the stand. It's black, with yellow accents, and blue ancil parts")

So, I grabbed the parts, selected the best cassette from the two that were hanging around (might use that wheel that I didn't choose the cassette from as a better wheel for this bike, but at the same time I think it's not better than a proper service on the current hub)

![](IMG_20230217_162103.jpg "Two old wheels next to each other")

![](IMG_20230217_162107.jpg "Up close to a 9 speed cassette showing very low wear")

![](IMG_20230217_162111.jpg "Up close to another 9 speed cassette showing more wear")

So uhh, there's a lot skipped here... In between those pictures a whole night happened. The actual changing of the shifter and cable went fine. I had to throw a new cable on, but it's the original cable and I've had this bike for 3.5 years now so like... Finally did it... But the chain was an issue... I thought it'd be fine, I've ran an 8 speed chain on a 9 speed bike before and it all goes well enough, but this time it didn't... Okay, so I throw a 9 speed chain on and it works fine! Beautiful. The chainline is wrong, but we fix that... There were issues with the chain... 

![](IMG_20230218_124604.jpg "The gears are on the bike, with the chain slung over the easiest gear")

![](IMG_20230218_124607.jpg "Looking down at the bottom bracket showing an awful chainline and a spacer. ")

![](IMG_20230218_131040.jpg "Spacer now removed, chainline is better. ")

![](IMG_20230218_131049.jpg "Spacers on the tray")

![](IMG_20230218_131051.jpg  "Showing a little more space on the non-drive side")

As I set off on this near chain I hear a grauncing. The chain is wrapping around the front chainring and back up until it gets hit off by the chainstay. I assumed this was because the chainring was an 8 speed one, and cheap and wasn't playing nice... I cleaned it and that helped, but it wasn't perfect. I took a deeper look and realised the chainring was worn to fuck. I put this down to it being cheap and was like, yeah, whatever... I cried a little, because honestly it'd taken over 5 hours of setup so far to get to this point, but as I had a nap I realised I could probably flip the chainring around. I'd already ordered a new chainring at this point though, but it'd be fine for a ride if the chainring didn't come before the next weekend. 

I have since realised that I had overstretched my old chain... I thought it was fine, but appently it was so dirty that it didn't let the chain guage in... That's bad, and that's my fault. I only know this after replacing the chain when going to the GT and the gears are skipping in gear 5 of the old cassette. It's this gear that I used at the park. It's clearly worn when looking at the cassette... As much as I bang on about cleaning chains, and not fucking up parts... I fucked up parts. I will endeavor to do better for my bike, because holy fucking hell I love riding this bike. 

I think I need to order a chain cleaning device, and more degreaser after I've written these blog posts... 

![](IMG_20230218_171630.jpg "Old setup with the gold chainring. I turned it around from previous to extend the life")

![](IMG_20230218_171635.jpg "Closeup on the gold chainring, it's really worn out")

Turning it around worked, but the new chainring came before the weekend, so I just swapped it out. It looks sick in blue, and I'll not fuck this one up (I hope)

![](IMG_20230220_161720.jpg "New raceface chainring in bluwu")

![](IMG_20230220_162743.jpg "New raceface chainring installed")

![](IMG_20230220_162750.jpg "9 speed shifter on the bike, looking at the bars")

So, I haven't yet ridden the bike with the forks installed, but I did put them on, and it's not enough for it's own post so uhh, yeah... New fork! Absolute steal off ebay for 52 quid for a nice fox float 32 from ages ago (IDK the year honestly, about 2010 though). There are however issues... The settings don't actually seem to do anything, so I might have a fucked damper... I'm gonna get it serviced, but until then it works smoothly and I can adjust stuff with just different pressures. It is a 60mm travel fork, down from the old 100mm travel, which does make the headtube angle steeper, but actually I think I like it so far, from the little ride I did to check everything was solid. The wheelbase is actually basically the same because the offset on the new fork is more than the old fork, so that maybe 1.5 degrees is offset by the offset! 

![](IMG_20230304_124622.jpg "The bike, in shadow, in the stand ready for the new fork")

![](IMG_20230304_131444.jpg "My bike, now with a blue chainring, and white and blue fork. It looks very fast.")

I think the bike looks absolutely fucking fantastic right now, and I can't wait to have the energy to ride it, hopefully next weekend! I'll take it to a shop that can service suspension at some point, but honestly I don't hold out much hope for that being until after the summer. I've got the [GT now (see other post here)](/mechanics/gt-retro-mtb)