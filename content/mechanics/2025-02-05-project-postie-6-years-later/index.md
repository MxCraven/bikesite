+++
title = "Project Postie Part 6: Years Later"
date = 2025-02-05
[taxonomies]
tags = ["mechanics", "project-postie", "text"]
+++

The [final post of Project Postie](/mechanics/project-postie-part-5) didn't come out as I really wanted, and that's weighed on me since. I just wanted to get the thing finished, so didn't take good pictures as I did it, and I didn't take any proper pictures after. Well, I now have that same Grandad's camera (very posh one), and have ridden the bike a few thousand KM to events like [Critical Mass](/rides/critical-mass-leicester-march) and shopping every week. So here's some nice photos, and a talk through of everything. 

### The Trailer

![A red bike with a black, silver, and yellow accented trailer attached lent up against a wall outside a bike shed](postie-trailer.jpg)

The trailer was an accident, well, the trailer itself wasn't but it was meant to go on my Boardman regular commuter bike, not this one! But the boardman, with the rack on the back, and mudguards, and just the way the whole thing was couldn't take the bracket for the trailer pin. All my other bikes have hooded dropouts. So the postie, with sliding dropout, was the only bike that could take it. I was going to set the Boardman to be able to take the trailer (light modification to the pin needed) but I just never did, and I think it's better that way. I've been able to ride this bike every week, and I think the postie gives a certain feeling when I ride that car drivers respect more. 

### The Sturmey

![A close up of a chain, rear cog, and large silver bike hub. It's a Sturmey Archer 3 speed AW hub. It's quite dirty](DSC_0054.JPG)

One of my favourite things I did was the gearing, it really helped with the trailer, but it's also made the 2nd gear the crusing gear, 1st is climbing, and 3rd is speed! All the way up to 25mph, maybe a touch more before spinning out. That's WAY fast enough for a cruiser commuter like this (the bike is happily stable until that fast though, it's a lovely ride). 

The Sturmey 3 speed has failed a few times due to not oiling it enough, but being an AW (Always Works) hub, it's come back easily after a little freshen up with WD40 and fresh lube (just using Finish Line wet, a great commuter lube, but horiffic to clean off as I find every time I have to use it on anything other than the postie). 

### The mudguards

![A view from the rear of a red bike as it leans against the wall](DSC_0057.JPG)

I don't think I noted this in the original finishing post for this bike, but the front mudguard didn't fit through the brakes, so I cut the head off of it, re-attached the black protector, and it looks smart and works great. The position is high up, and the rack between you and the wheel means the spray never touches you, and the mudguard comes far enough down to protect the bottom. 

### The Brakes

![A close up of the front brake of the bike. There's a lot of grime around the frame, a small progress pride flag on the fork crown (drive side), and you can see a lock hanging from the rack above](DSC_0055.JPG)

Welding on those brakes was an amazing idea, the wheel build has been great. I really enjoyed doing it, and they've only needed a slight truing since. The rear wheel has *slight* play that I do eventually need to sort, but the front wheel has been perfect, and it's now spaced out enough on the fork to fit easily if I need to take it off. The brake pads are about to wear out, and hopefully the next pair can reach, since the brakes are just a touch too long for where the posts are (it's a tough thing to get right)

### Tyres

These tyres were just some I had lying around from when the Boardman ran some old MTB rims and was 26" (the wonders of Discs). Those wheels need repairing so I can have nicer wheels on my jump trail bike, but that's been "in the works" for a few years now... It'll happen, maybe... They've been amazing tyres, they're cheap but hardly showing any wear after a year on the boardman and a few years on this bike. I've had 2 puncutures, one caused by off roading, that fixed itself due to putting mucoff slime innertube goo into the tubes, and another one because the valve split (that tube came with the bike and was at least 10 years old). One on the rear, one on the front. Not bad for the riding I've done. This bike, always reliable, always got me home (the split tube happened overnight, though I did have to ride my CX bike to work because I'd broken the Boardman with a big crash that yeah, I really should have posted about)

### The Toolkit

![A pic of the front of the bike, showing a metal rack with a wire base, and an elastic net over the top, with a small roll of cloth held down by straps to the rack](DSC_0056.JPG)

The original toolkit was stuffed into whatever I could get onto the bike, but then I decided to get a piece of waxed canvas cloth, and nick some toestraps off another bike (a Trek I'll be posting about soon). Wrapped all the tools into that (a small multitool, mostly for others bikes since I only need the screwdriver, adjustable spanner for most of the bike, tube, patches, and a pump) and strap it to the rack and it's been amazing. So far I've been fortunate and only needed the spanner to tighten the wheel every once in a while (the rack pulls the wheel to one side sometimes) and the pump (for the aforementioned puncuture above)

### Broken things

So what broke in all this time? Well, very little. I've gone through at least 3 rear wheel washers (I'm sure I'm doing something wrong, I don't know what), the brake pads aren't set right (hopefully to be fixed soon), the bottom bracket has developed play twice (which isn't too bad, though I need to fix the 2nd time soon), and a cotter pin basically fell out (but I did get it back working). Other than that I really can't think of anything that's gone terribly with the bike. It's ever reliable, always there for me, and hasn't let me down, and in true to this bike fashion, I haven't washed it once! It got a bath when the country flooded a few times (including a complete underwater ride through the lakes, where I rode through some of the lakes themselves as they overtopped!)

### The Future

![A side view of the bike, red, with light blue pedals and grips. A black saddle hangs a light that looks like a pair of bollocks. There are silver mudguards, spokes, and hubs, the chain sags slightly (as was the style at the time).](DSC_0052.JPG)

I'm just going to keep riding and loving this bike, it does everything I need it to do and I love it to bits. The best bike in the world, and the coolest thing I'll probably ever build or ride. Thanks Grandad. 
