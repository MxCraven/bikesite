 +++
title = "Spring Time Service Time"

[taxonomies]
tags = ["text", "mechanics", "service", "cx"]
+++

This is a late night post, but I keep forgetting to write it! Not much will make sense, but when does it ever?

It was the clock changing day last week, also known as 6 months since I installed my wrap around mudguards on my CX/Road/Gravel bike. That's when I said I'd take them off. Also a great time for a full service. 

![](IMG_20230325_120700.jpg "A drop barred bike with purple, yellow accents and white and black main colouring. It has wrap around mudguards and is quite dirty")

Chain has lasted a year, it holds up to the 1% checker, and I'm sure this checker is a bit too soon, but I shagged a cassette trusting the other one too much, so now I'll be a safe enby.
![](IMG_20230325_121333.jpg "A chainchecker showing the chain is worn to 0.75% Replacement time")

Had to try and get the not-quite-roundness out of the wheel. It worked to some extent. The bulge was at the seam, which is slightly worrying
![](IMG_20230325_122720.jpg "A rear wheel in a truing stand")

Cleaning down the bike was quite satisfying. Getting all the crap and grime out of the bike
![](IMG_20230325_130844.jpg "The bike in a stand, sans wheels and mudguards")

There were 3 major rain showers in the washing of the bike, and that;s why there's no further pictures, but all in it took 3ish hours, I stripped down the whole bike and cleaned it, put a new chain on, and sorted the bearings in the rear wheel not quite being right and the untrueness of the wheel.
![](IMG_20230325_130847.jpg "Parts like rear mech and cranks and cassette on the cleaning table")

The issues that occured weren't really with the found issues on top of a standard service, it was more just cleaning everything took a long time, plus rain showers and stuff. If I were to do it again I'd leave the rear mech on the bike, just so I didn't have to resetup the gears (I left the front one on). I'd still take the jockey wheels off and clean those and the cage. 

I'm not quite sure if I'd do all the work to the brakes I did. In this service I sanded down the disks, and pads, and put grease on the backs of the pads as often suggested. The brakes are taking a while to bed back in, but I'm sure it'll be fine again soon. 

I think next service I'll have to do the cables, although that won't be too bad, and at some point over the summer I certainly need to get a hydraulic bleed kit and service the brakes. They're not bad at all, but I can definitely feel some spongeyness, certainly from the rear.

Anyway, that's the basic service I did. I definitely missed some stuff here, but it was an enjoyable way to spend some time and the bike runs absolutely amazingly! Can't feel the difference between mudguards and not, but there's something cool about the look with both, and I like it. 
![](IMG_20230325_150620.jpg "The bike again, cleaner and reassembled" )