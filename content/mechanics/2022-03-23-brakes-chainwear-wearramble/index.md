+++
title = "Wear and Tear Parts"
date = 2022-03-23T16:45:00
[taxonomies]
tags = ["ramble", "mechanics", "text"]
+++

I've had a few random mechanics things go off over the past weeks that I've not put on here, but before that if you're wondering what's going on, why this is a more bloggy post, and where project postie is, well uhh, I felt like it, and the guy who's welding my frame got ill and had to pospone giving me the bike back. 

So I decided it was finally time to change my brake pads on my MTB. Some may remember the last time I said that, where I decided they were fine [(see here)](/mechanics/brake-pads/), well this time I felt them go bad, couldn't get them that good, and decided it was time to just replace them whatever they look like. I have done so, and am glad I did. This is what they looked like: 
![](IMG_20220319_114610.jpg "Some disc brake pads from the side. One is significantly smaller than the other")
![](IMG_20220319_114616.jpg "The same brake pads from overhead")

There's still some life left in those, but certainly not much. The performance after isn't much better than before, but the feel of the brakes is better, and I'm sure these pads will last a while. 

Another pad related thing happened on a bike I repaired to sell for my Dad, the front brake pads were worn. First time working on road pads ever, was good. I'm only saying this because "Things come in twos" for me right now. 

When I was riding at the park on my MTB my pedal started wobbling. The bolt had come loose. This is an issue at the park, as my crank bolt takes a 4.5mm allen, and I only had 4 or 5 on my multitool. Luckily I'd already stripped the head and made it fit a 5 while trying to use a torque wrench [(see here)](/mechanics/success-crank-swap/). I tightened it back up with some natural threadlock (grit) and it's held fine. I should replace the bolt as it was quite stripped on the threads when I got it, but I have executive dysfuntion so... yeah

![](IMG_20220312_155923.jpg "A blue and red Trek road racer. The tyres are blue, as is the bar tape. It's lent up against a brick wall.") (I did the bar tape on that, and I'm very proud because I did the figure 8 perfectly on my 50th attempt! I now don't like sticky backed tape because it's harder to do that on, and impossible to take off) After changing the road pads on this bike I flipped the stem on my gravel/cross/road bike, because I felt I'd like a more upright position as I head towards trying to ride it offroad more. I forgot to take a picture of this. If a mirracle happens I might update this post at some point with a picture. Once I'd fiddled with that I rode the bike up and down the lane to see if the bars were back in the right place for me, and if it was worth doing. I think it was, but we'll see on longer rides. I did notice a slightly worrying wobble from the cranks though... The same one I'd felt on the weekend with my MTB... Ah, crank is loose... Shit. This time it wasn't stripped, but does take a weird star bit that you need to get specifically (which sucks if it ever did come loose on a ride! Nobody is going to be carrying a Shimano crank pre-load tool in their spares kit on a Cafe stop...) 

Anyway, with all those things fixed, I stopped for the evening, after checking a few chains for wear of course (I said this would be a ramble) We're time travelling back in time to the week before the events of this (and these events have jumped massively, hope this makes any sense at all really)

While cleaning the Trek I measured the chain, and decided for shits and giggs to just check the other chains. Oh fuck, my road chain was past 0.75... Oh fuck, past 1.0... That's bad. It's 11 speed, so in theory I shouldn't go past 0.5, and 1.0 is a death sentence for the whole drivetrain, right? (nah, it's fine, I got there in time)

I did immediately take the chain off and order a new one (which always annoys me for going 11 speed. 10 speed chains are so much cheaper). This led me to worry more about chains, after [this](/mechanics/chain-wear/) (wear and tear is a thing I love, but also hate, but that's a whole nother post). A few posts on Fedi later and I now understand the sudden jump. The pins are hardened, and once the harden (0.1mm thick, or less) is gone, you're into soft steel and boom it's work way fast. 

My new strategy for chains though is to wipe with a rag after every ride (or every week, on my commuter, as that does 30 miles a week). I have no idea if this will do very much, but it did make the chain *way* easier to clean on both my MTB and commuter at the weekend, so I'm going to keep doing it for sure. I think it will help life though as I checked my collegues bike, who had washed at the same time I did and rides roughly the same roads at the same time and the same distance, and the lube that I got on my finger from a quick rub was just as black and horrible, but far far less gritty on my bike than on his. 

So uhh, yeah. Long post, lots of ramble, if you got this far holy shit I'm so so sorry, but uhh... Cya out on bikes! It's getting warmer, so let's get out there :3 
