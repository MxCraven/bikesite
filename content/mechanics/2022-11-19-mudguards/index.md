 +++
title = "Mudguards, my saviour of bum"

[taxonomies]
tags = ["text", "mechanics"]
+++

I finally got mudguards (like 4 weeks ago, I also finally made the pissing post...) Honestly I swear I've had no things to post or something IDK, let's ignore that there's no fucking algorithm here! 

So the mudguards: They needed to fit within the not massive clearance of my cyclocross bike. It's got clearance for 35mm tyres with good mud clearance, but mud clearance isn't needed to the sides as long as the tubes are designed to aid dropping the clumps. So I chose the SKS Edge AL 46(mm outside width). They fit what I need. 

![My Postie bike with a large box strapped with a yellow truck tie](IMG_20221028_151929.jpg)
Gotta make use of the postie!

![My CX bike in a stand. It's got a black frame with yellow and silver accents. The mudguard on the rear is a straight silver bit of plastic.](IMG_20221028_152143.jpg)
This is how the bike started out. With the mudflap that protected my rear on dodgy days and light crap in the spring and summer, and also all last winter. But it's not good for the front, or the guy behind. Plus I want to ride off road more this winter (if I can find anywhere that's not just a bog)

![The front of the bike with mudguard attached. It's got at least 1.5mm clearance](IMG_20221028_154337.jpg)
When fitting the front I completely forgot about turning the fork so I could fit my allen key in... I was doing 1/4th turns for AGES! Had to bend the sides in slightly for it to fit, but that's expected with these things. It fits really snug and clear gap all the way around. 
![A further pic of the front wheel all covered up with mudguard](IMG_20221028_154338.jpg)

I had to fiddle a lot with getting the right spacers in all the places, and bending in what needed to be bent but you can't even tell. I made sure to regrease every time I took them out though. REALLY don't want that shit seazing. 
![My bike now with 2 black mudguards wrapping the tyres.](IMG_20221028_162959.jpg)
I like how the bike looks with the guards on. The missing saddle bag helps the clean look, but I've been told by multiple people they didn't realise I even had them on. I did want something a bit more flash (silver) but hey, these are really solid. 

![The bike this time against the wall but with a nice little saddle bag attached](IMG_20221028_164250.jpg)
Told you there was a saddle bag

![A close up of the rear wheel showing increased clearance on the old setup. Now there's plenty to go with off the beaten trails ;)](IMG_20221112_131632.jpg)
There was a slight clearance issue at the back which caused rubbing after a small bit of mud got stuck. The guards recommend 1.5cm of clearance all around. I did not have that at all, but now I've got it, which is really good for running these offroad :3 

Overall, these are REALLY good mudguards. I can recommend them so far. Easy to fit, come with plenty of bolts, and while I wouldn't call this a review I'd give them a solid score if it was. Time will tell how they hold up, but I've washed the bike by leaning it against a brick wall on the back wheel, and it's not scratched the paint, plus all the other leanings and scuffs the bike has come across in about 150 miles of wet road riding and a touch of off roading. 

Hopefully I've got some more stuff to post soon. I'm currently midway through a wheel hub repair (low on pics, because it's so muddy) and I'm not sure what to do next really... I enjoy writing these though. I just need ideas :3 