+++
title = "Brifter Broke? Going Oldschool"
date = 2022-01-01T16:57:35Z
[taxonomies]
tags = ["mechanics", "audio"]
+++

{{ audio(path="friction-shift.mp3") }}
Changing gears the old way is good!

Fig. 01
![](IMG_20211231_111159.jpg "The purple wrapped handlebars of a cyclocross bike in a stand. There's a bar bag, and a plastic plug in the bar")

Fig. 02
![](IMG_20211231_111208.jpg  "The front mech with a cable with a blue end")

Fig. 03
![](IMG_20211231_113045.jpg "A view of the bars with the barend shifter installed, no cable in it. It's pointed upwards ready for the cable to be installed")

Fig. 04
![](IMG_20211231_114239.jpg  "The cable installed into the barend shifter. It's routed down into the frame.")

Fig. 05
![](IMG_20211231_114242.jpg "The cable coming out the bottom of the bike, near the front mech")

Fig. 06
![](IMG_20211231_115330.jpg "The cable has been routed under the bottom bracket shell, and threaded into the front mech. It's even got a cable end!")

Fig. 07
![](IMG_20211231_115331.jpg "The bar end shifter, in the bike, set to the big front ring position, which is roughly horisontal.")

