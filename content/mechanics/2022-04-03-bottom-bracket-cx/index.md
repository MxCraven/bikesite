+++
title = "🥺 Brackets"
date = "2022-04-03T16:55:47"
[taxonomies]
tags = ["text", "mechanics"]
+++

I've never been quite sure how well the previous two owners of my Scott CX bike looked after it, but I'm 99% sure this one was my fault. From a [recent post](/mechanics/brakes-chainwear-wearramble/) someone might have read and remember me talking about my crank coming loose? Well, that seems to have been a long term issue, as the bottom bracket was creaking and still loose as we headed out on Tuesday night for the first evening ride of the year. I got back, tried to sort it, and found the non-drive side bearing was absolutely shagged... It's pretty much frozen solid, and the drive side feels like someone packed it with sand instead of grease. 

![](IMG_20220401_163708.jpg "A very dirty non-drive side hollowtech 2 bottom bracket in a bike. There's a wear ring on the outside metal. You can see all the way though as the cranks are off")

It'd also worn a groove into my NDS crank, which I was worried would be an issue with the new BB (if I was missing a spacer)

![](IMG_20220401_163616.jpg "A black crank with two rings where the paint is worn. One is quite deep, and only runs half way around, the other is smaller in the middle and supposed to be there")

Annoyingly I stripped out my hex key 5mm while doing this. It served me well. Now I'll need to buy another set (I have one in my basket but descision paralysis, and expensive, and also deciding whether to buy knockoff crankbros cleats as I'm not getting on so well with Shimano right now)

So I ordered a new BB on Tuesday, and hoped it'd be here on Friday, and that the weather Friday night would hold off long enough for an install ready for Saturday morning. 

All of that happened, somehow! I got a new Ultegra BBR60, which is slightly smaller around the outside than the old Hollowtech 2 thing (but Hollowtech 2 cranks are still used, and fit and that's now what the bearing size standard is called, god damn it why did they have to make it so complicated for barely any bloody gains, it's probably saved like 8 grams). Luckily my tool did have both sides on it, so it's fine! 

![](IMG_20220401_164247.jpg "A black BB in the packet on the left is slightly smaller than the old silver BB on the right (which is dirty)")

I cleaned out the threads, added a bit of grease (I didn't trust the amount of grease on the threads of this BB, it's not a lot so I replaced it, but what was inside for the spindle was quite a lot and some had to be wiped off after pushing the crank through). Then I installed it clean, torqued it down to "quite hard" and put the crank in. I had to hit the drive side crank (the one with the spindle attached) quite hard with the rubber mallet to get it in. This is the same as on my MTB which I [changed here](/mechanics/success-crank-swap/). I guess new BBs are just a tighter fit than really well worn ones? 

Anyway, after getting it in I cranked down the bolts with a torque wrench (partially to stop the cranks coming loose, and partially because I'd stripped the end of my 5mm which was easiest to fit and do those bolts up)

![](IMG_20220401_165031.jpg "A blury picture of a torque wrench set between 12 and 14 nM, and a sticker that tells me to do so on the crank arm")

You might be able to see the rain starting to come down in that picture. It just held off until I was almost done, and at least until I was past the point where it mattered (putting on grease in the rain is a right pain in the arse, and I was already in waterproofs from commuting home because it was supposed to piss it down and had been for the 30 mins before we left). 

The difference in looks isn't much, but it does look sleek in black with the frame and being slightly thinner than the old

![](IMG_20220401_165401.jpg "The new BB installed from the non-drive side. Light spots of rain cover the bike")

The main difference is that I can feel it's smoother while pedalling. It was immediately obvious when I rode it out on Saturday, so I hope I can keep this one in good working order for at least 10,000km or so. My commuter bike is still going strong and feeling smooth (even with the cranks out and my fingers in the greasy bearings) after probably more than that (honestly not sure when that thing was changed, it might be factory, the shift cable probably is). 

So here's to long bottom bracket life, and the ease of changing a threaded bottom bracket! I wish all the repairs I do to things were that easy, and obvious. 

Cheers for reading this junk. Cya next time. 