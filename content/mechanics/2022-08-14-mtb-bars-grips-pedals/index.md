 
+++
title = "New Touchpoints on the MTB"
date = "2022-08-14T17:00:00"
[taxonomies]
tags = ["text", "mechanics", "mtb"]
+++

![](IMG_20220805_164609.jpg "My MTB with orange grips and pedals. THe bike is mostly black")
This is how my bike looked before. I went into the orange colour as my old tyres were yellow, and that made the whole bike look a yellowy orange colour. However it was only the tyres keeping it together, and with the grips being definitely worn, and the pedal pins in need of replacement I decided it was high time for some nice shit on my MTB. This is also while I was looking for BMX wheels and to get back on that steed, so realising how much I'd put into upgrades for that thing and barely ridden it, I knew I really really had to show my MTB grind bike some love. 

![](IMG_20220805_164615.jpg "A closeup of the orange pedal, you can see it's very worn")
![](IMG_20220805_164618.jpg "A closeup of the old orange grips. You can see the file pattern on the palm has completely worn away smooth, and that the end is pretty ripped from crashes and drops. ")
![](IMG_20220805_164729.jpg "Comparing the size of the new pedals to the old. The new ones are about 5mm bigger in both directions. ")

Grips: DMR Deathgrips Miami, a stunning blue and pink marble colour. I got them in thicc, because I've had some thumb ball bone hurty issues recently and knew bigger would help that. The old ones were thin, and unflanged. Not sure how I feel about the flange with the gears, but it's not big so I like it. Definitely love it on my BMX where no gears, one brake, and it's large. 

![](IMG_20220805_170706.jpg "The new grips on the bike, showing a cockpit view. They're blue and pink marble effect and slightly bigger.")

Pedals are DMR v11 in sky blue. The old pedals were off ebay and fairly cheap. They're good, but they're not reliable, had a lot of wobble, and I can't find pins for them. When I bought them I said "I'll buy new, proper pedals if I end up being a full time flats rider" (as this was when I was still deciding if flats or clips were best, and as I rode more pump track and freestyle shit I realised definitely clips can get fucked. Still ride them for CX and road though, but if I had to lose them for any reason I'd not be sorry). The new pedals are a bit bigger, but not too much. 

![](IMG_20220805_170711.jpg "The overall of the new grips and pedals. It changes the colour of the bike completely and makes the gold/orange more of an accent colour while the blue pops. ")

The whole thing looks bloody lovely, and felt amazing. A few days later some new bars came for it too. I'd been looking for some slight riser bars for a bit, and some 35mm risers came up at a good price and I snagged them :3 They make the bike look so much better, and I think they make me ride slightly better too! I could probably go higher though, maybe I will at some point (although I've been saying I'd get some riser bars for like 6 months now...)

![](IMG_20220810_165328.jpg "The new bars with the bike hanging on the wall. They're riser bars so they sweep up slightly.")
![](IMG_20220814_084630.jpg "The bike full side on at the track lent against the tree stump. It doesn't show much, but I liked the shot of it, with the nice background and the saddle bag removed. ")

So yeah, gave the MTB some love like I said I would. Today I learned fast plants on it, and I'm getting slightly slowly more confident with doing no footers (very tiny ones) and did a few one handers on jumps I've found too sketchy or not big enough to take the hand off before, so that's going really well. Watching Crankworx and the European BMX champs definitely helped (especially making me just want to learn fast plants)

So yeah, super sick. Cya next time. Keep shreddin!