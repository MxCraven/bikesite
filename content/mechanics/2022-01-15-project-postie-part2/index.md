+++
title = "Project Postie Part 2 | Wheel Dismantling"
date = 2022-01-15T17:29:14Z
[taxonomies]
tags = ["project-postie", "mechanics", "audio"]
+++

{{ audio(path="part2.mp3") }}
Part 2 of a journey of building bikes. This time we take apart the wheels! Use the tags to see all posts!

[All Project Postie Things](/tags/project-postie)


![](IMG_20220109_141437.jpg "An orange bike with no front wheel lent against a car. It's a GT Karakoram")

![](IMG_20220109_141441.jpg "A front wheel sat on a table with the rim tape to the side")

![](IMG_20220109_143429.jpg "The rim with no hub in it. The hub is next to the rim with all the spokes poking out of it")

![](IMG_20220109_143915.jpg "The front wheel in bits, with a pile of spokes, a box of nipples, a strip of rim tape, and a hub inside the rim")

![](IMG_20220109_150650.jpg "The hubs of both wheels and their spokes sat on the table")

![](IMG_20220109_154216.jpg "The front of the GT in the stand")

![](IMG_20220109_160715.jpg "The wheel rims sat next to the tyres that will go on them")

![](IMG_20220115_110757.jpg "The rear wheel of the postie bike with one brake pad removed, the other still sat there. It's a centre pull rod brake system")

![](IMG_20220115_110801.jpg "The front wheel on the floor, out of the bike")

![](IMG_20220115_111246.jpg "Both wheels on the floor out of the bike")

![](IMG_20220115_111253.jpg "The bike in the stand with no wheels")

![](IMG_20220115_113312.jpg "The front hub with removed axel. It's very dirty")

![](IMG_20220115_113320.jpg "The axel on the table with dust caps. It's all incredibly dirty")

![](IMG_20220115_113649.jpg "The cone from the axel. It has been worn away and broken. It's no longer coned")

![](IMG_20220115_113829.jpg "Checking the bearing tracks in the hub for pitting with a sharp screwdriver")

![](IMG_20220115_114238.jpg "The inside of the rim painted red over the spoke nipples, likely for rust protection")

![](IMG_20220115_114241.jpg "Black electrical tape that must be 40 years old used as rim tape")

![](IMG_20220115_114834.jpg "A pile of old electrical tape on the table")

![](IMG_20220115_114836.jpg "The rim without the electrical tape")

![](IMG_20220115_122058.jpg "The front hub, trying to get a picture of the name which I still can't make out")

![](IMG_20220115_122631.jpg "The rear wheel has shaved off ends of spokes and spoke nipple spacers.")

![](IMG_20220115_122636.jpg "Same as previous, but other spokes")

![](IMG_20220115_125739.jpg "The 3 speed sturmey archer AW hub gear out of the bike")

![](IMG_20220115_125742.jpg "The print on the hub saying 'Made in England' and '(19)86 - 5 (May)")

