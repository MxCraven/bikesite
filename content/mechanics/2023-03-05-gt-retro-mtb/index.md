 +++
title = "New old steed :3"

[taxonomies]
tags = ["text", "mechanics", "retro-mod", "hackyshit"]
+++

Some of you may remember the [Project Postie](/tags/project-postie) [wheel donor](/mechanics/project-postie-part2/) bike... Well, it's been hanging from my garage roof for a year now and I just loved the look of the frame, and definitely had to do something with it, and FINALLY I did... Welcome to the family, the Neon Annihilator.

I never mentioned, but the non-drive side bottom bracket part was stuck in there. It'd been driven in too far, and without grease. This was a bugger to get out.

![](IMG_20230214_162602.jpg "The bottom bracket of a GT Karakoram upside down")

![](IMG_20230214_162608.jpg "Showing the mangled and stuck bottom bracket part")

I tried putting the BB tool in the vice and using the full leaverage of the bike, but I couldn't get the downwards pressure and it was basically stuck fast. I had to resort to a really long extender bar, a hammer, and praying the BB moved before the breaker bar broke... It did, fortunately, but I had to remove the most of the way by doing 1/4th turns at a time as I had to still use my full weight to move the part! Use grease damn it!

![](IMG_20230214_162612.jpg "The BB tool is in the vice on the table")

![](IMG_20230214_163928.jpg "The stuck part is out, and I've got the rest of the BB")

Here's the bucket of parts, I didn't end up using the levers as the ones on the donor Marin were very nice, and these are quite cheapo ones. The shifter was put onto my main MTB (see that [upgrade here](/mechanics/9-speed-mtb)). I used the old 8 speed I took off of that. It was nicer than the shifter on the Marin, which was quite bulky and the screen was very dirty. 

![](IMG_20230214_164437.jpg "A bucket of parts including cranks, pedals, levers, and the 9 speed shifter I didn't use off to one side")

![](IMG_20230214_164923.jpg "The bike upside down in all is unglory")

![](IMG_20230219_120446.jpg "Our new donor bike, a silver marin with blue forks")

Getting the front brakes on the bike was a PITA... They had this weird thing on them to hold the spring that made setup a nightmare since it was friction fit. Why didn't they just pop the spring through the little hole in the mounts I don't know, but they seem to work good now. The rear brake sucks, it's a U brake, and IDK how they work. It barely springs back, and I think I'm gonna have to spend an afternoon fiddling with it at some point. 

![](IMG_20230219_124330.jpg "The GT in the stand, no wheels, but it does have front brakes on it. ")

![](IMG_20230219_124334.jpg "A close up of said front brakes on the RockShox Indi fork")

One afternoon I spent the time just assembling this bike. I'd had enough of gears after doing the 9 speed MTB so I left it here. Doing things in stages is just so much better for me right now. Break everything down into small parts and just say "if the vibes are right, keep going" was how this bike was built. 

![](IMG_20230219_135610.jpg "The GT is mostly assembled, no chain, no brake cables, and with the seat down. The broken down Marin is just outside of shot")

![](IMG_20230219_135621.jpg "A front angle of the GT")

I came back from work and said, I'll throw cables onto the brakes. If the back break doesn't cooperate, or I can't figure it out, that's fine just leave it. It cooperated enough. 

![](IMG_20230302_162759.jpg "Chain attached and brake cables added to the GT")

![](IMG_20230302_170753.jpg "Gears done on the GT, the bike is nearly finished")

I was only going to throw the gear cable onto the bike at this point, but the vibes were right so I went whole hog and set it up. It took a little bit of fiddling but it set pretty well. A few skips in harder gears, but that turns out to be because the chain was shagged... 

I changed that, since I had some 8 speed chains laying around from when I found them for a fiver (thanks [BikeBargains](https://www.bikebargains.co.uk/)) All in this bike cost a chain and £3 for a QR seat clamp which has yet to arrive. Might cost a little more when I get around to fixing the broken star nut (yeah, it's slipping out so I can't properly tighten the stem down, but it's janky enough to be safe :P)

The 5th cog is fucked, as the chain was really far gone actually, and just too dirty to put in. I might take the old 8 speed cassette out of the parts bin, but it's only got a 30t on it, where this has a 34t which is way better (even if it technically doesn't fit at all, even with the B limit maxed it hits unless the chain is on and 1 half link too short)

The bike works though. I've taken it around the test circuit at the park near my house, and I'll at some point soon take it around a route. I probably need to find a bottle cage in the garage, and any one of the tens of old pump holders we've got. I'll have to temporarily nab a pump each ride, but there's a patch kit in that old saddle bag (thanks grandad <3) and even if the saddle bag rattles because it was modified to fit a different bike (also thanks Grandad :P) it looks quite cool on this bike I think.

We'll see how she steams in future. I envisage it as a cross country bike, that I can also loan to friends or ride myself as they take my NS. It's quite twitchy at the front, but it bunny hops well and I think can totally send! 

Anyway, I'll leave you with the bike, and yeah... Have a good one, thanks for reading! 

![](IMG_20230302_170810.jpg "A front view")

![](IMG_20230303_173510.jpg "The very nearly final bike. An orange GT Karakoram with yellow tyres, orange grips and pedals, and an old worn out saddle bag on the back for style points")

