+++
title = "Perplexed by Chain Wear"
date = 2022-02-12T13:54:00
[taxonomies]
tags = ["mechanics", "text"]
+++

I've never quite known how chain wear works, or how the hell people know if they've done 1500 miles or whatever (before the days of Strava and having bikes on there especially). I do want to keep my bikes in working order, so check my chains, but when they're about to wear I get paranoid that 2 pedal strokes over the magic 0.75 (I stick to this even on 11 speed, due to not having a 0.5 wear gague) will ruin my chainrings, cassette, and everything. This hasn't happened so far, but it still worries me and I end up checking every day as it gets near. 

My commuter has been in a strange state however. For a while, when measuring from the inner links (as supposed) the 0.75 almost goes through, but doesn't. Nearly there, but not time to change (and it's 7 speed, so certainly not). However measuring from the outer links, which [Park Tool's guide](https://www.parktool.com/blog/repair-help/when-to-replace-a-chain-on-a-bicycle) says not to do, due to it potentially reading unworn when the chain is worn from the tool resting on a plate and not the rollers, it will drop in straight away. I even made sure that it was definitely holding on the rollers, and not any plates myself. 

I was very confused, and can only assume that it's because of the narrow wide chainring, but have no proof of this. I checked the chain after my ride today and found it dropped through 0.75 on both inner and outer links, which means I had full right to change the chain and have peace of mind again. Still goes through 1.0 on the outer plates and not on the inners though. 

I'll keep an eye on the new chain as it wears, and also my mountain bike with a narrow wide on it. All the chains we have in the shed, with 1000+ miles on, and regular same tooth chainrings have the same measurements from both inner and outer links, so maybe there is something there, and in another year I'll report back. If this chain can last 1500 miles it'll be a year before it wears, so here's hoping it's closer to 2 years. I take good enough care of it, I think (it's washed every 2-4 weeks, which at 32ish miles a week is 60-120 miles, but the dodgy conditions through the winter don't do it much good). 

The chain I previously had was a Shimano Altus 9 speed. A good chain, but without nickel plating it did surface rust, which asside from looking ugly might have made the chain wear faster (with rust particles getting into the rollers and such). Who knows? I got a KMC Z 8 speed chain for the bike (runs 7 speed, so can take a 7-8-9 speed chain) as it was the cheapest chain at the time the old one was almost worn out. I like KMC chains. They're cheap and perform well, plus they make their 8 speed and lower end chains to the same standard as the 11 and 12 speed ones, which is great when you ride reasonably priced bikes, or want a chain on your commuter, likely your most ridden bike and in the worst conditions, to be of really high quality, but run cheaper parts on it (1x7 is more than enough for my commute)

Anyway, there's some ramblings on chain wear and the weird stuff it has. Time to add the new chain to my bike on Strava so I can track the wear on this one properly, instead of guessing based on where I can remember going (which on a commuter, is kinda difficult when you just pop out for random runs) 

Hope anyone who follows me on Fedi is glad this didn't get posted over there across 400 toots!
