 +++
title = "One Gear, One Problem"

[taxonomies]
tags = ["text", "mechanics", "singlespeed"]
+++

So first of all, no I've not yet finished the [GT](/mechanics/gt-retro-mtb) and fully tested it... The starnut is still loose, the 5th gear still skips. I'll do it at some point, honest! 

But I've got a new idea, see?! And it requires changing wheel sizes (like almost all my plans seem to)

We've had this old green bike in the house for ages. It was a project bike for my Dad, who repainted it and made it quite nice, but he could never make it work. The rear mech never quite stuck (no hanger, the holding piece doesn't stay put), and the gearing is old, massive, and crap. It was meant to be a pub bike for him, but if you can't ride it when sober it's gonna go badly... 

It does however have skinny tubes, sliding dropouts, and a lot of chromed parts :3 

![](IMG_20230409_094413.jpg "A green bike hangs in the stand with one wheel in the front, and the chain and gears flopping low")

The bike is technically complete, I just forgot to take a pic before I whipped the wheels out, as I was excited to check sizing and all that. 

The brakes *might* just about reach, we'll see if they do, but if they don't I'll only need longer standard brakes, not the full on longreach which is quite nice. Roughly 65mm needed, by my calculations. 

![](IMG_20230409_093835.jpg "A wheel in a bike with a rule against it measuring 60mmish from brake bolt to wheel rim side")

I've taken stock of what parts I need. 

- Bearings for the rear wheel (1/4th in)
- Singlespeed kit
- Pedals (beartrap cages?)
- Chainring bolt spacers (for when I've removed one or the other chainring)
- QR skewers, probably best to go bolt on so I can crank down the rear wheel, and also for style points

As for work things that I've noted:

- Respace the rear wheel dropout (135mm I think)
- Remove the granny brakes
- Check the bottom bracket (Dad said it's jammed in improperly)

![](IMG_20230409_094419.jpg "Frontal shot of a bicycle with black bartape and granny brakes on the drop bars")

All of this seems like a good project that'll make a nice bike. I'm not sure quite what gearing I'll go for, but I know there's a 52t and 40t chainring to pick from. 

![](IMG_20230409_094406.jpg "Closeup of the crankset and chainring of the bike")

The tyres will be 28mm that I have lying around, and the wheels are donated by the same guy who buggered [this wheel](/mechanics/siezed-wheel/). The rear has the same exact issue, and the front had a big ol' dent! 

![](IMG_20230409_094647.jpg "A bike wheel with a big dent in the side of it")

But we fixed that with some plyers. It's not perfect because the rag I was using to try to protect the sidewall tore through, but it should work fine. These rims have plenty of wear life left in them, and hell the rear one is nearly brand new somehow. Maybe he just used the drag from the locked up rear hub to do his braking!

![](IMG_20230409_095059.jpg "The same rim showing scratches, but straightened out")
![](IMG_20230409_095106.jpg "Scratches from the plyers on the braking surface of the rim")

Sheldon Brown of course held the answers here. His [guide to roadside wheel repairs](https://sheldonbrown.com/on-road-wheel-repairs.html) is pretty much the only place I could find that dared tell you this sort of thing is possible, other than a really old book which also stated the same sort of methods as Sheldon. 

So, that's the next project! It's gonna take a bit of work to make nice, but I think it'll be a cool addition to have a true singlespeed to go around on, and having a "racer" is something I keep looking at, but know I'll never need because of the glory of wide tyres and low tread CX bikes! 

Just gotta bite the bullet and start doing the work. Spreading the rear triangle will come first, and after that I'll pick up the parts I need. 

As a bonus, I finally made the rear wheel on my MTB properly tight. The bearings are shit, I probably need to replace the whole axle unfortunately, but for now it can stay... I don't have the energy... 
![](IMG_20230409_092542.jpg "Rear MTB wheel sitting on the floor")

