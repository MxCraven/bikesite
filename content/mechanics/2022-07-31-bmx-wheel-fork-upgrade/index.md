+++
title = "Finally Fixing my BMX"
date = 2022-07-31T00:00:00
[taxonomies]
tags = ["audio", "mechanics", "bmx"]
+++

{{ audio(path="untitled.mp3") }}

![](IMG_20220731_102044.jpg "A BMX driver cog tooth thing with the bearing press")
![](IMG_20220731_104829.jpg "A BMX wheel in the frame with the bearings shreded drive side")
![](IMG_20220731_105522.jpg "The shredded bearings outside of the bike")
![](IMG_20220731_110720.jpg "The driver is back in the bike, no bearings, and as tight as it can be")
![](IMG_20220731_111212.jpg "Some pedals, chainring, and cranks on the floor, taken off the bike")
![](IMG_20220731_111220.jpg "Some half sanded, badly spraypainted white BMX cranks")
![](IMG_20220731_121754.jpg "A shot of the front of a BMX bike with the tyre having a gnats butthole of clearance between itself and the top of the fork")
![](IMG_20220731_121809.jpg "The rear end of a BMX bike, new black sprocket, shinyish silver cranks, and a quite loose chain")
![](IMG_20220731_121748.jpg "The overall of my BMX bike. Mostly green and black, with a blueish grey frame. It's a MafiaBikes Kush 2+")

