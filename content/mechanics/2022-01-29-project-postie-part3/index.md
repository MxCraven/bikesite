+++
title = "Project Postie Part 3 | Teardown"
date = 2022-01-29T15:33:17Z
[taxonomies]
tags = ["project-postie", "mechanics", "audio"]
+++

{{ audio(path="part3.mp3") }}
Part 3 of a journey of building bikes. The frame is in bits, and we're getting closer. Use the tags to see all posts!

[All Project Postie Things](/tags/project-postie)

![](IMG_20220123_122745.jpg "A red bike with no wheels sits in a bike stand")

![](IMG_20220123_132838.jpg "Same bike, now missing front mudguard, chain, and one handlebar grip")

![](IMG_20220123_133930.jpg "The cranks removed from the bike, the bottom bracket is covered in shit")

![](IMG_20220123_133932.jpg "The bike with no cranks on, fully in frame")

![](IMG_20220123_134553.jpg "The bottom bracket non drive side is out. There's a lot of muddy grease in there, but it's smooth and clean")

![](IMG_20220123_140741.jpg "The bike has now lost the handlebars")
