+++
title = "Project Postie Part 4: Frame Updates"
date = 2022-04-13T18:30:00
[taxonomies]
tags = ["audio", "project-postie"]
+++

{{ audio(path="audio.mp3") }}
The frame is back, and I've started sanding it down! 

[LINK Alfano Frameworks](https://www.alfanoframeworks.com/)  
The framebuilder who brazed on the brake bosses for me <3 
 
![](IMG_20220326_134717.jpg "Red postie frame lays on the ground. There's missing paint around the yolk and brakes")

![](IMG_20220326_134723.jpg "A close up of the brake mounts that have been welded onto the fork")

![](IMG_20220326_134727.jpg "A closeup of the mounts on the rear seatstays")

![](IMG_20220326_134730.jpg "A closeup of the yolk (bit behind the bottom brackets/where the cranks attach) where a new bar has been added")

![](IMG_20220412_170009.jpg "A flapwheel in a drill and a knife covered in red paint")

![](IMG_20220412_170013.jpg "The first marks of sanding as I tried a few methods")

![](IMG_20220413_131936.jpg "A roughly sanded main triangle with the forks and stays less sanded")

![](IMG_20220413_131938.jpg "Closeup on the downtube that's reasonably well sanded")

