+++
title = "New Parts Haul"
date = 2022-03-10T18:55:00
[taxonomies]
tags = ["new-crap", "audio", "mechanics", "ramble"]
+++

{{ audio(path="audio.mp3") }}

This one is mostly a ramble, but there's some mechanics in there somewhere

Fig. 1
![](IMG_20220303_161824.jpg "My MTB in the stand. It's got yellow topped tyres with black sidewalls")
Fig. 2
![](IMG_20220303_161843.jpg "A closeup of the rear tyre, it's almbost bald down the centre")
Fig. 3
![](IMG_20220303_161847.jpg "A close up of the front tyre, it's quite good")
Fig. 4
![](IMG_20220303_162501.jpg "The rear tyre is off the wheel, and the new tyre sits next to the rim. It's got tanwall, offwhiteish sidewalls and black top")
Fig. 5
![](IMG_20220303_165425.jpg "The front wheel in the bike with new tyre on")
Fig. 6
![](IMG_20220303_165723.jpg "The bike standing with the new tyres on")
Fig. 7
![](IMG_20220304_164306.jpg "A rack loosely lain over my silver commuter bike")
Fig. 8
![](IMG_20220304_165149.jpg "The rack is now attached on one side with some P clamps")
Fig. 9
![](IMG_20220304_165151.jpg "The rack is partially attached at the top, near the seat")
Fig. 10
![](IMG_20220304_172612.jpg "The saddle bag doesn't fit with the rack added as it's too high")
Fig. 11
![](IMG_20220304_173846.jpg "The saddlebag is now mounted under the rack with some velcro straps")
Fig. 12
![](IMG_20220304_173850.jpg "The velcro up close")
Fig. 13
![](IMG_20220304_173853.jpg "The velcro bodge up close and from the side")
Fig. 14
![](IMG_20220304_173858.jpg "The bike with the rack attached, looking lovely")
Fig. 15
![](IMG_20220307_163032.jpg "The rack with 2 large cardboard tubes loaded on it, strapped down with a basic cargo net")
Fig. 16
![](IMG_20220227_145311.jpg "Some used Peaty's Brushes. There's a flat one, a curved thin one, a long thin one with a funny blue head, and a toilet brush one")
