 +++
title = "Changing Brakes, Mangled Starnuts, and Tight Chainring Gaps"

[taxonomies]
tags = ["text", "mechanics", "gt", "mtb"]
+++

I finally test rode the [GT retro XC bike](/mechanics/gt-retro-mtb/) and it's amazing, but the rear brake was completely locked up. The springs are gone, and so I bought a new one. 

Forgot to take pictures though, so all you get is the after of a new BMX U brake installed on the bike. It's not great power, especially compared to the old one (but the old one never fully released it's power)

![](IMG_20230513_124513.jpg "The rear of an orange bike with yellow tyres and a new brake")

The second issue with the bike was a mangled starnut that pulled upwards when tightening down the cap, meaning the headset was never tight, which is quite dangerous. 

Here's a quick guide on how to replace a bollocksed starnut. First, remove the parts of the starnut
![](IMG_20230513_124744.jpg "A bolt in a mangled bit of starnut")

![](IMG_20230513_124750.jpg "The starnut minus the thread still in the stem")

![](IMG_20230513_124834.jpg "Starnut parts and the topcap on the floor")

Next, aquire a starnut installing thwacker, and a starnut (usually bundled cheaply online)
![](IMG_20230513_125047.jpg "Starnut installer on the bike")

Hit it. 
![](IMG_20230513_125118.jpg "Starnut installer has been thwacked with hammer")

Yeah, it was a really easy process and the whole thing sets nice now. Gonna full test the bike again this weekend with a longer one and muse about the next issue the bike has. Tyres. I want that bike to be a winter off road bike, which means it'll need some more tread than the worn Kenda Smallblocks it's got, but the issue is the clearance is at best 2.1" and finding good XC tyres in 26x2" (for clearance of mud) is very hard right now. I'm thinking my best bet is the Conti CrossKing, that's the style I want, but there's so many others I think I like the look of more that I just can't run because they don't come in the right size. Strangely Schwalbe, who are usually good with many many sizes start all their MTB 26" tyres at 2.25" which is bigger than I can even put in the front of the GT! Really 26" tyres are for dirt jumpers and retro bikes (or kids XC bikes) so make them 2", that's great for DJ and fits retro bikes really well. Bigger is better, but mud clearance on a U brake fuckin sucks!

I did a bit of a change to the dirt jump/trail masher/non-XC bike too. The chainline was crap, so I was going to try and move the chainring to the inner position on the chainset (it's a 3 chainring setup, so inner big is the middle, therefore should be a proper line) and I was sure I'd need a spacer as my old 36t chainring rubbed before, but apparently it's *just* fine. 
NS
![](IMG_20230513_125814.jpg "")

Such a tight gap there... 
![](IMG_20230513_130349.jpg)

I've got a spacer somewhere I know just in case, but it gives a perfect chainline to the 5th gear, which is the skatepark gear so I don't want to push it over an extra gear if I don't have to. But it's nice to not hear grinding when in the 1st gear. 

Anyway, bikes cool, hopefully I'll get to ride them soon, need to get the fork serviced on the jumper but I don't want to lose the bike just in case I really want to ride it and yeah... 

Bikes are cool, next up writing a post on the M check, and how I wash bikes. 