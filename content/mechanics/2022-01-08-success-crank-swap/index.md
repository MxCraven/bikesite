+++
title = "Crank Swap Saga Part 2: The Swappening"
date = 2022-01-08T14:03:34Z
[taxonomies]
tags = ["mechanics", "audio"]
+++

{{ audio(path="success-crank-swap.mp3") }}


[Part 1 Here](/mechanics/failed-crank-swap "Part 1")

![](IMG_20220108_124412.jpg "A gold coloured chainring being held. It's got 'Snail' written on it, and 104BCD")

![](IMG_20220108_124801.jpg "The parts needed for the project laid out on the table. Chainring, cranks, allen keys")

![](IMG_20220108_125631.jpg "The chainring with some bolts through and washers")

![](IMG_20220108_125825.jpg "The chainring attached to the crank, the washers are now the other side")

![](IMG_20220108_130005.jpg "The front side of the chainring, showing it's towards the back of the crankset")

![](IMG_20220108_131748.jpg "Two cars and a driveway. There's a shitload of rain coming down. This picture was taken from the safety of the garage after rushing everything inside")

![](IMG_20220108_132438.jpg "Pedals are now attached to the cranks on the floor of the garage")

![](IMG_20220108_132536.jpg "Hitting in the cranks with a rubber mallet")

![](IMG_20220108_132634.jpg "A shot of the chainring showing it very close to the chainstays of the frame")

![](IMG_20220108_133204.jpg "Swapping the chainring to the outside of the cranks for more space")

![](IMG_20220108_133428.jpg "The pedal end of the crank is going to rub on the chainstay now")

![](IMG_20220108_134013.jpg "The same shot as before, but there is a good amount of clearance between the crank arm and the chainstay")

![](IMG_20220108_134018.jpg "A shot down on top of the bottom bracket showing the spacer added")

![](IMG_20220108_134452.jpg "The non drive side crank is now on. The lock bolt has been stripped slightly")

![](IMG_20220108_134617.jpg "The chain is on the bike, it's glorious sunshine, and everything is back together")

