 
+++
title = "Wheels! BMX! Bearings!"
date = "2022-08-14T17:00:00"
[taxonomies]
tags = ["audio", "mechanics", "BMX"]
+++

I'm sorry if you don't listen to the audio on these, there's kinda no pictures of this shit...

{{ audio(path="untitled.mp3") }}

![](IMG_20220811_170106.jpg "Close up of my old BMX wheel (the salt one) with 2 spokes missing from the drive side")
![](IMG_20220811_170050.jpg "The new wheels on the bike. My bike is a blue grey colour, with green accents. The wheels are silver hubbed and black rimmed (although the black paint on the rear is coming off from braking, and coming off fast)")

Cheers! <3 