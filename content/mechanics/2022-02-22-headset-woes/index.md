+++
title = "Headset Woes"
date = 2022-02-22T18:00:00
[taxonomies]
tags = ["mechanics", "text"]
+++

I bloody hate bikes sometimes. It's often said that you experience just as many issues on Linux as on Windows or Mac, but that when you know you can't fix them you ignore them and move on, and I have the same relationship with bikes now... I want my shit to run perfect, but usually make it worse, or try a much more advanced thing instead of something simple. 

Case in point, the currently ongoing saga with my cross bike cum gravel bike's headset. It felt a bit shitty, so I took a look inside. The horrors of no grease, and a lot of rust met me, so I tried cleaning them and made things a little better, but also a bit worse as while trying to clean the rusted bearing cage I bent one... But I went to a bike shop, and bought some more for £1 (god love [Bob Warners](/rides/bob-warners-trip) ). They were too big, so I took to the Internet, not heeding the warning from John (the mechanic often mistaken for Bob) that there's 5 different sizes. I ordered what SJS had as their only caged headset bearings, they said "Fits most 1 1/8ths headsets", and they're weldtite so they're not shite. 

They didn't fit... Too small... I've measured the outside diameter of the ones that do fit and came with the bike, and they're 40mm wide, which is what most sites selling the Weldtite ones say they should be (if they bother to say the actual bearing size at all). 

It's all very confusing to me, but after regreasing the bearings and putting them back in, they're smooth again, so who knows what the issue was... They're pitted so I should replace them soon, but considering there's no proper seal on the bottom bearing I think I'm going to buy an integrated cartridge headset next time I have some money to spend on that bike. I'm so glad my MTB came with one, and the same with my commuter. 

Anyway, uhh, this is a post, I don't know how to end, thanks for reading, sorry for no pictures, and I'll figure something else to post out in the future...
