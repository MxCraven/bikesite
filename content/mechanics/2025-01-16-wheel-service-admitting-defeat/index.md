 +++
title = "Admitting Defeat, and Letting a Shop Service My Wheel"
date = 2025-01-16

draft = false

[taxonomies]
tags = ["text", "mechanics"]
+++

Wobbling wheels, I have them often. On my dirt jump/trail mix bike, on my commuter (although that one is stable), on the wheel I repaired [a million times (and more after that)](/mechanics/cross-bike-upgrades), and finally after about a year on the new wheels I didn't mention because I wasn't posting. Now normally I'd just jump straight into it with making the repairs myself, but damn am I glad for once I decided not to. 

The wheel in question is a Vision Team 30, a wheel you can't find on the Internet because there's too many variations of them. This wheel is extra special though, and (despite being early days, I know) got the award for "Weirdest Thing I've Seen This Year" from the bike shop [Northside Bikes](https://www.northsidebikes.org/). 

 **A weird wheel**

First off, the skewer isn't a normal small skewer that goes through a hole in the axle, the skewer is the axle, almost like a thru-axle except not because it's actually quick release. Then, the axle inside the hub that the bearings run on, that has cups and locknut type thread on parts to hold the bearing on. There's a flange and flat on this centre piece "axle holder" thing, then bearing, then holding cap flange (threaded on), then locknut that's the piece that touches the frame (that's non drive side, hidden by the disc brake). 

On the other side there's a hand tightened thread on cap into the freehub, that part is quite simple, but it hides a dark secret. You can't knock the bearing out because it's like =: where the bearing is flush with the inside of the hub shell, and so you need a fancy puller. So in that moment, I knew I was definitely glad I chose to take it to a shop. The break even price on shop work for this vs doing it myself was only a missread bearing away from costing more at home, and a specialty tool definitely throws that right out. 

So we took it over to the recommended [Julies Cycles](https://www.juliescycles.co.uk/). Recommended by the aforementioned Northside as someone with higher end tools, and by friends we ride with who've bought bikes from there (particularly Derek who had hub troubles himself due to some Bontragers being poorly assembled). When I went in they were massively overworried about not being able to fit it in! 

**You all need to slow down**

Eventually, when they said actual dates, it was revealed that "We might not be able to do it today, but definitely tomorrow" was the actual time constraint, which for me is more than more than fine. I was prepared to wait a week to allow for ordering in of bearings (so many different standards and sizes, and some hubs use 2-3 different sizes, all this and you're unlikely to have the size in stock at any time). For me, it's the 16th of Jan as I write this, but on the 16th of Feb I've got an Audax [(Rutland and Beyond, something I should have written about riding last year)](https://www.audax.uk/event-details/12511-rutland_and_beyond), that I need the bike to be in good working order for. So that's kinda why I decided it was time to get the wheel serviced. The play had started last year, in around November, so I just decided to shag it through the winter but have it serviced for this. 

Anyway, I digress into what I didn't do, not what I did. So I realistically was fine with two weeks for the service. That'd still give me two weeks with the bike before the event, and I'm certainly not short of bikes to keep the fitness up on in the meantime.

Despite making all the worry about time constraints and their workload (I get it, I also don't like to be pushed, and always underpromise and overdeliver), and me saying I don't need it done in a rush, I'm texted 3 hours later saying it's done (annoying, no price stated in that, and no hard price at the shop as they didn't know bearing costs until the bearings had been looked at to see what they even were, a ballpark of £30 for labour, and £6-10 per bearing x 3 or 4 bearings was given). Well, I can't even pick it up until Tuesday! (Out doing things Friday, Saturday I want to ride, Sunday I could maybe go, Monday they're closed, Tuesday I can get taken by Dad instead of riding through town with a wheel on my back). 

**What now?**

After all that craziness, the next question was what do I ride. The simple answer being either go on the MTB, take a weekend break (not gonna happen with the bad weather before this weekend having 3 enforced no cycles, and two enforced no run/cycle), or pinch Dads bike. As it happens, my Scott and Dads Cube are very similar. 2016 for me, 2015 for him, both 11 speed (mine with an Ultegra mech, but 105 level everything else and his 105 throughout), same cassette, same brand of brake pads, so I said well, 50/50 his wheel might fit in my bike. It does. Perfectly. 

So now I have my Dads wheel on my bike, and all is well until I can collect my wheel again. He's even got the same tyre that I run (Schwalbe CX Comp, a love letter I need to write to them). 

So uhh, yeah, thanks for reading, hopefully more to come. GG
