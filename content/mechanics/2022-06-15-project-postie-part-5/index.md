+++
title = "Project Postie Part 5: The Finale Finally!"
date = 2022-06-15T17:00:00
[taxonomies]
tags = ["mechanics", "project-postie", "audio"]
+++

{{ audio(path="part5.mp3") }}

It's finally, finally, done...

![](IMG_20220603_152838.jpg "The postie bike, almost entirely rebuilt. The bike is together and the cables and chain just need sorting")

![](IMG_20220605_132821.jpg "The bike only needs a chain. The mudguards and rack are on now")

![](IMG_20220614_202526.jpg "The complete bike after it's first ride. Red frame with blue accents. Everything is there. It's a crap photo")
