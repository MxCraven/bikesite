+++
title = "An Imperial Hundo, New Parts, and Good Riding"
date = 2023-01-02T14:00:00
[taxonomies]
tags = ["rides", "audio", "mtb", "new-parts"]
+++

{{ audio(path="untitled.mp3") }}

Quite a long listen this one, get a beverage of choice and enjoy it~ 

![](IMG_20221221_081814.jpg "A picture of a bike computer in the day before the 100mile ride. It reads 5 seconds, 0mph, 72ft moved, time of day 08:18:13")

![](IMG_20221221_163926.jpg "The bike computer after the 100 mile ride. It's dark, the light is on the bike, it reads 8h21m17s, 101miles, time of day 16:39:25 and 1450m of elevation")


![](IMG_20221226_113850.jpg "A geometric orange and blue mudguard on a black MTB fork")
![](IMG_20221226_113856.jpg "Another angle of the same mudguard")

![](IMG_20221225_200931.jpg "A shiny brass cycling bell labelled 'LION ENGLAND'")

![](IMG_20221226_115040.jpg "The same bell mounted on the stem of my bike. ")
![](IMG_20221226_115043.jpg "The bike from afar, with the bell glistening in the sun")

![](IMG_20221228_145228.jpg "Post llangedla mud on the back of my yellow raincoat. It's all over it, covering the front too")

![](IMG_20221231_114824.jpg "A bluey grey BMX with green accents and purple pedals")

