+++
title = "Bike Rack Sadness"
date = 2022-01-15T18:10:00Z
[taxonomies]
tags = ["rides", "angy", "bike-commuting", "text"]
+++

This is a written post without audio description.   
  
Today I had to go to the opticians, so of course I rode my bike. It's only in town. Where am I gonna park? Well, the car park oposite has the bike racks, now they've been removed from the main shopping centre part (which has a no bikes part, but the parking was just outside it on the main street).   
  
I rode down, perfectly normal. Parked up, and realised something. The rack I was parked against was held in by bolts that weren't welded. If that rack was full all it'd take to have all the bikes on it, no matter how strong their locks are, was a 15 or 17mm socket set or just an adjustable spanner.   
  
Your bike lock is only as strong as the thing it's locked up against, and this just is a terrible thing to have honestly. It's not a proper way to do things, it's a cheap option that barely helps anyone since there's a fence right behind that *is* stronger bolted to the floor...   
  
  I fucking hate councils and cheaping out on bike facilities  
  
Why does nobody cycle in this town? Maybe it's because it's a nightmare, there's cars constantly pulling out into the middle of the road due to inadequate parking, most of the bike racks are completely out of the way for where you want to go, and to enter it from either end of the mainstreet there are absolutely abismal roundabouts that nobody has a clue what anyone else is doing, and indications are a suggestion to most. Plus it's a place with a thousand blind spots, and people get confused when you actually go around the roundabout rather than cutting across it (which on a wet day like today was, on a slippery bit of white paint I wasn't going to do)  
  
It does get my gander right up that they put some effort in, but clearly didn't think about it at all and made the place more unsafe because people feel safe with it there. 

Nobody will care when someone's bike does get stolen because of it, and they'll use that to justify less bikes around. 

One day the future will be beautiful and full of bikes.   
  
Since this is written, have some quotes I found! [fmoisan](https://sites.google.com/site/fmoisan/Personal/Cycling "fmoisan's 'Cycling or the noblest invention of mankind!") &  [BikeRadar](https://www.bikeradar.com/features/inspirational-cycling-quotes/)
  
> “Bicycling is a big part of the future. It has to be. There’s something wrong with a society that drives a car to work out in a gym." - Bill Nye -

> "Whoever invented the bicycle deserves the thanks of humanity." - Charles Beresford -

> “The bicycle is the most civilised conveyance known to man. Other forms of transport grow daily more nightmarish. Only the bicycle remains pure in heart.” — Iris Murdoch, Irish author

> “Those who wish to control their own lives and move beyond existence as mere clients and consumers — those people ride a bike.” — Wolfgang Sachs, German author and academic

> "I ride my bicycle to ride my bicycle" - Clipped from a zen proverb (that might be some bollocks a commenter made up, but sometimes WillyBumArse2001 comes up with something good)

> "Money can't buy happiness but it can buy a bike and that pretty close" - Unknown

![](photo_2022-01-15_18-16-05.jpg "My commuter bike lent against a bike rack in a car park")

![](photo_2022-01-15_18-16-10.jpg "Said bike rack shifted off the ground, showing it's only held by unwelded 15/17mm bolts that can be removed with any spanner")
