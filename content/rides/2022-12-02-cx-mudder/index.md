 
+++
title = "Stuck in the Mud"
date = "2022-12-02T20:30:00"
[taxonomies]
tags = ["text", "rides", "cyclocross", "bike-wash"]
+++

So I went for a ride in the mud. I had a half day off work on Wednesday and I decided to *really* test the semi slick, light gravel/summer grass, but mostly for road tyres I have (the WTB Byway)

![](wtbbyway.png "A semi slick gravel tyre. It's got a smooth patch down the middle, then slightly treaded around, then goes to some thick tooth file, and finally big nobs on the outside")

These tyres are definitely NOT rated for what I did, but they held up *perfectly*. They even held, just about, going over slippery bog grass, and over thick mud. The only place they didn't cope was when the mud was clagy and thick. They didn't clear well, and the mudguards didn't help either, but I've had far better tyres for the mud get stuck on literally ploughed fields like that. I wish there were more tyres like this, because the rear is wearing and I'm worried my next ones will have the same wobble issues these ones have. 

![](IMG_20221201_131401.jpg "A picture of a black and yellow bike in a field, lent up against a style. There's big trees, and a babbling brook. In the background there's an old pidgeon house and a church steeple")

This was a beautiful little place to find after a long steady descent. Not sure how well the tyres might have held up at 15mph+ with any turning, but this wasn't at all a race so I could always just slow down instead of going down. 

The thing that really muddied the bike was the 2nd to last off road segment. This is a well trodden bridleway in the trees, and it was FUCKED. It was covered in thick mud, and little streams going through it, and everything. Where it was good, it was amazing, but with the leaves mixing with the mud my wheels seazed so hard twice I had to remove the wheels to unclog the guards! I'd possibly have been able to push through that with the tyres, but the bike clogs in those conditions without mudguards so the guards have been fantastic, and as you can see from the next picture, they kept the bike kinda clean (other than the wheels, which were submerged in mud multiple times, along with having blocks of mud run along them)

![](IMG_20221201_142806.jpg "A black, yellow, white, and purple bicycle covered in mud. Mostly on the wheels and pedals, as it has mudguards")

Cleaning the bike from this, to the next pic took roughly 45 minutes, about 4 buckets of water, plus a sprayer. All was collected from outside buckets and rainwater collections though! 

First I took the wheels out and hit the bike all over with the bog brush, it got most of the mud off. Then I went smaller with the brushes, used the sprayers, and finally finished with a clean bucket where I sponged down any remaining debris and muck. 

After all that was done, I still gave it a casual 10 minute clean (along with my commuter bike) which made sure the chain was clean, and the bike sparkled. 

I gotta eventually do a "How to clean bike in 10 mins" post, because too many people have dirty bikes! But I'm sleepy, and currently half awake watching the European MTB champs (from 3 months ago)

![](IMG_20221201_153429.jpg "The same bike, but sparkly clean. The ground is wet underneith it")

![](IMG_20221201_153434.jpg "A close up of the clean chain and rear wheel where most of the damage was in the mud")

![](IMG_20221201_153439.jpg "The mess of cleaning supplies, brushes, sprayers, buckets, and sponges on the floor near an open garage door")

So yeah, cyclocross is incredibly fun! I love riding it, love watching it, and the mudguards have made it even more possible (because I stay dry and clean ish, and the bike is less of a complete fucker to clean)

Bonus pic :3
![](IMG_20221202_145732.jpg "A bike lent against a skate ramp in the distance. There's a small pump track of dirt cut into the lush green grass.")
