+++
title = "Critical Mass Leicester Isn't Dead!"
[taxonomies]
tags = ["rides", "text", "CriticalMassLeicester", "CriticalMass"]
+++

It is *not* dead, and it's an amazing thing and great fun! 

On the last Friday of the month, at 18:00, I met up with a few riders in Orton Square Leicester for an unplanned and open to all bike ride! 

There were only 5 people there (including me), but I'll take that with the weather being quite shit. 

My journey over was a chill 6 mile ride up a main road, but I was definitely ready for better bike lanes and the safety of a group once I got to the city. I rode the [postie bike](/tags/project-postie/) which was the perfect bike for the ride. A comfortable commuter, and absolute thrill of a party bike. Sure some hills were a bit steep, and I did start losing gears as the wheel slipped, but damn it's a comfy bike around town, and the reliability of thick tyres with puncture goop just made me feel safer. I'm going to add a waxed canvas roll of tools, ala [eric marth](https://www.youtube.com/c/ericmarth)

The ride was about an hour and a half, only travelling 6 miles, and taking in some nice sights of the city. I only got a picture of this one: 

![](shaun.png "A wall painted with Shaun the Sheep and his dog mate, where the characters are unwrapping to reveal non-cartoon/claymation dogs and sheep underneith.")

There's a lot of cool shit in cities, and a lot of pride flags, which was awesome to see on Trans Day of Visibility (where I was, of course, not that visible... Well, except for the high vis and extreme levels of lights I've got on the bike)

I can highly recommend you find out about your local critical mass and go down, and if it doesn't exist maybe consider starting one if you feel your city isn't safe to cycle on your own. If your city is friendly and safe to bikes, please be thankful for that, use it, and have a bike party instead! Celebrate the bike when you can, and fight for the ability to ride safely. 

Critical Mass is awesome, and more people should do it. Show people there is an alternative, and have a great bike ride at the same time!