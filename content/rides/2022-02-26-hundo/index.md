+++
title = "Hundo rides suck and mean nothing"
date = 2022-02-26T15:51:00
[taxonomies]
tags = ["rides", "hot-take-titles", "text"]
+++

Okay, so I admit this title feels very much like a huge hot take, as I write this just after I've got back and my legs ache, and it *kind of* is, but at the same time ask me in a week and I'll still agree, and maybe in a few months I'll pine for another hundo but not once I've got half way round. 

My average ride is 2 hours, my average distance (road) is 30 miles (50km). Why did I want to do a hundo then? Well, it's customary to do the years of your age in miles (or KM if you can't be arsed to do miles, and are over 55) for your birthday. I'm 22 (soon, as of writing) though, so that's not really a big ride... Today we did 21.3 to the cafe stop while I did the first part with the group ride. 

100kms just take time though. Time, and planning, and eating properly, which makes them harder for me than for most people really. I decided to do this one in two parts. First the normal Saturday ride, which would take me to about 35 miles (out of the 62.137 required), and the other 50km would be on my own. 

I filled two bottles, added some special electrolite tablet to one of them, and headed out for the first bit, which included a cafe stop, as previously mentioned. CW for the next paragraph as I talk about the cafe stop, and food. 

{{ spoiler(spoiler="I don't often stop for the cafe stop, as usually I have somewhere to be, or don't want to be out for that long. Bikes good, the social part not so much. This time I had to stop, because I was hungry and that means you're in a bad place. So we stopped, and I had a thing called a 'Cup Cookie' (and a hot chocolate). The cup cookie is a mug shaped biscuit, covered in marshmallows, and filled with chocolate. The chocolate at the bottom of it though, I thought was just a chocolate moose. Turns out, nah. It's basically nutella mixed with sugar. Holy shit that was sweet. The biscuit was lovely, but damn if I didn't need the energy for the long ride I wouldn't have eaten the whole cookie. I'd highly recommend not giving a small child more than a small spoonful of this stuff because it was potent") }}

During the cafe stop I also managed to call someone old, her partner young despite him being older, and honestly social gatherings just aren't my thing so I'll probably avoid the cafe stop in future, opting instead to come home early and watch the weekends bike racing. 

We now head into the second half of the ride. Normal bike rides head out with the wind in your face, so it's giving you a push home. My double loop tactic would mean that just as I finished my normal distance, I'd get home, drop the empty bottle off, have a piss, and go out again, but into the wind when into a vaguely unknown and certainly rarely trodden place for me. 

The route I picked was a fairly standard one for me though. Taking roughly 26 miles, it should put me up at 60, which I could bump the last few required for 100km quite easily with a small extention to the end (which I did). Unfortunately, it's also a route which goes out up hill exclusively for 13 miles... Into a headwind, that sucked after already finishing my regular ride, but better than coming home into a headwind for sure. I like the route normally though, because it's all downhill on the way home, and as soon as I reached the top and started going down with the wind on my back, that was wonderful and I stopped being sad, literally caught my second wind, and zoomed towards home. 

At 52 miles I was coming through a small village and realised my bottle was almost empty. I spotted a few people at the village hall and asked if I could borrow their tap. They kindly let me (thanks, I'll come down if you've got a school fete going on and I hear about it), and one older chap seemed to know bikes a bit. Asked if I was doing Paris-Nice next weekend, which isn't a race the average non-cyclist has heard of (and it is starting next week too!). After they asked how far I was riding, and I'd answered I was doing 100km I got the standard answer of "Well that's not very far!". This, during my homeward bound, was where I really started thinking about this title... 100km rides, and 100 mile rides, really are pointless. They look impressive, but plenty of people do them all the time, and people who regularly ride 30 miles at the weekend, or commute 5-10km each way to work are generally fit enough to do one at any time they like, assuming they plan it out, stop for food somewhere, and have all day to do it. 

Distance = Speed * Time, which means Distance = Time if you don't give a shit about speed. 

I am glad I did it, it was nice to be out, and I can say I've done it and not do it again for a while. Tomorrow I'm doing 22 miles at the pump track, which will actually be a challenge, especially after this. It's a lot of laps, and I only get 7 miles out of the commute (3.5 each way)

As I came home I passed a sign I've seen a lot, but not taken a photo of, so here it is: Knotting Hill! 

![](IMG_20220226_143920.jpg "A photo of a {cute} white enby in front of a sign that says 'Knotting Hill'. The sign is wooden and pyro engraved")

![](IMG_20220226_143926.jpg "The same sign, but instead of me, you can see my bike, which has the enby colours in it and is much better looking than me.")

All in all, it was a good ride, and I am glad I did it, but I can't see myself doing that sort of thing regularly. I much prefer the dirt, and mixing it up, and doing short stuff with techy gibs. Maybe if I ever manage to retire I'll do some offroad 100km rides, but I always end up thinking "It's easier by road, and most people count it as the same anyway"... 

So? The Ride By The Numbers? (thanks DKlein, [link here](https://youtu.be/HDGcWAI3r7k?t=204))

![](ridebythenumbers.png "The Ride by the Numbers in black stamp letter writing on a paperish background")

64.45 miles (103.72km)
4h49m10s rolling time (6h27m18s elapsed)
1099m of elevation (3,606ft)
and for the TT hunter in me, 7 PBs on segments, including the official hill climb course up Polly Botts! 

So yeah, that was good, thanks for reading, get out on your bike whether this sounds like fun or not, because it doesn't really matter how far you go, just that you do <3 
