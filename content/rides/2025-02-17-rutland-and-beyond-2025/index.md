+++
title = "Rutland and Beyond Audax 2025"
[taxonomies]
tags = ["rides", "text", "audax", "rides"]
+++

### What's this then? 
An audax I should have written about last year when I first did it, but I didn't so here we are back again! Last year it was raining, and there were many floods, this year it was snowing to start, and freezing cold! Always a good test of kit then. 

I decided to do this again pretty much right after I finished it last year, it was a great thing to get me back into the year and caused me to go ham with longer rides, up to 200km, including rides to Hunstanton for 160km. Most of my road riding last year was just gently chugging along at an easy pace, which was amazing. 

This year I want to go faster though, time trials and off road segment hunting! Still, a mid Feb 100km audax is a great ice breaker to get started I feel. 

### Preparations
Last year I prepared well, doing 50km each Sunday and whatever TdQ rides threw at me. This year I just did whatever in general, less seriousness but keeping my pace happily slow. I had a few worries 2 days before when I did a 45km ride looping close to the start of this ride, and it wasn't great, but on the day I ended up fine. 

My fuelling was to overload in the bottles. 60g sugar per bottle, a 550ml insulated, and 750ml regular steel bottle. This was a great plan, as it was hard to get sweets out the front bag with huge gloves on (and double gloved). In that front bag I had a pack of Haribo and some [home made flapjack](/other/flapjacks)

I'd been checking the weather for weeks, following the long forecast and then the short, all the way to the day before. Making sure also to check each of the stops around the route to avoid running foul of "it's sunny all day at home, but raining heavily where we're going" as happened at least twice last year. It was supposed to be entirely dry, then the day before I went for an easy 20km ride and was told it was going to be sleeting tomorrow. I need to get that guys weather app, because it was right! Cold and sleet at the start, though I've commuted enough to not really notice my Dad beside me was complaining. 

That chap also told me my seat is pointing upwards, which I'm really annoyed at because I've never felt it as a problem in the at least 2 years it's been set like that. All of last years rides, this one included, the 200km, the multiple 100km rides, at least 10,000km since I changed the angle (though I did lightly lower it 2 weeks ago, but I'm not sure if that was a true lowering or just resetting it to where I'd put it back too high after removing and regreasing for the anual "don't sieze the seatpost" check). So I'll have to get that pointed down flat, because now I do notice it. 

### Starting Out
![A touring style bike loaded with kit lent against a fense facing out ready to ride](photo_2025-02-16_18-16-00-bike.jpg "A touring style bike loaded with kit lent against a fense facing out ready to ride")

The first thing is to head off to the ride, it's 104km around the route but it's only an extra 8km each way for me to get there and home. Leaving just after sunrise. It was dry and no rain as I set off, then shortly after it started sleeting as I was waiting to meet with Dad. The next 20 minutes to get in were cold and sleety, but I'd put 2 coats, a thick gillet, and armwarmers on. So I was dry, and mostly warm other than my face. Double gloves were definitely the right move, but less for the sleet and more for the bitter winds later on. 

Turned up at the start hut, met Lou and Nick hanging around outside, for some reason in the cold, and went in to get signed on, collect the cards, and grab a snack before the start time. We arrived easily with 20 minutes before the proper start. 

### Off we go! 
We set off just after 08:30, which beats our last Audax at the end of last year where we only made it home with 15 minutes to spare, but also set out 20 minutes after the start time... The cutoff wasn't a worry, but it's always best to leave yourself more time. 

The first section of the ride is back the way we came, and towards our regular stomping ground of Borrough Hill, stopping to collect the first answer on the card. If you've not seen Audaxing before, it's long distance anti-race cycling. You ride around a route, collecting checkpoints, and some of those checkpoints (controls) are info controls. The first here asked "what colour are the birds on the Carrington Arms coat of arms?". The answer I leave you to find in the photo below. 

![A photo of a white walled pub called 'The Carington Arms'. The crest is made up of a horse, a strange blue bird thing with something around it's neck, lots of feather like things, and the main shield is black in the upper left and lower right corners, with white stripe, and black diamonds diagonally across it. Then the other two corners are St Georges Cross, white background, red plus, with blue peacocks in each segment.](photo_2025-02-16_18-16-00-ca.jpg "A photo of a white walled pub called 'The Carington Arms'. The crest is made up of a horse, a strange blue bird thing with something around it's neck, lots of feather like things, and the main shield is black in the upper left and lower right corners, with white stripe, and black diamonds diagonally across it. Then the other two corners are St Georges Cross, white background, red plus, with blue peacocks in each segment.")

### To the first proper stop
Off up the hill we went after that, it'd got a little warmer, and Nick and Lou shot off to stay warmer. I held back, knowing I needed to do a job of towing my Dad and Dharms around a little bit. It was a strong headwind. I knew we had plenty of time, 12kmh was an easy target for us, and the start is the hardest section so even if we did get to the first stop slow it'd be fine, we could make it up. 

The climb was good, I dragged Dharms and Dad all the way in my wheel and they seemed to be within breath and riding fine. At the top I had my 3rd leak of the ride, not sure if it was just drinking (enough) or if there's something wrong, but I'll keep an eye on it. Chased back onto the group, and rode around to get back to pushing the wind out the way. Holding a steady pace for the next section. Heading down the back of the hill, over the next 3km we made up 1kmh which was impressive! 

We rolled our way into the first stop at Rutland water with a little confusion. Last year the cafe at the first stop was closed, so the control was right near the entrance. I hadn't noticed that detail, so hadn't updated my GPS. It still led to the toilets (4) so that was good, and we asked and checked maps, and kept rolling around the lakes on the path. A beautiful section. I managed to get a bit of grass cross riding and gate keeping open done, which satisfied the part of me that keeps the off road tyres on my mostly used for road cyclocross touring bike. 

Into the first stop and cafe with plenty of time to spare! We got signed in and stopped for food. The chips were great! :3 Hot chocolate was too hot to drink though, so I ended up popping it into my thermal bottle (a great choice, and I'd emptied it already as was the plan to have it finished at least by then. Probably not quite fast enough though). 

### Onwards, to stop 2!

After a fairly long stop we were on 14kmh average as we left, perfectly fine. The next section around Rutland Water was cold with the wind smashing us, and fun because of some nice gravel sections. Overlapping and cutting across different groups as different people held open gates for each other, classic audaxing comradery. 

After we left the lakes is where the ride did start to get harder for some. Lou and Nick had already been yoyoing, but staying within sight of me, Dad, and Dharms, but now they were slowing going out of sight (not hard due to the turns and winds). This bit of the route is very up and down in the worst way, steeply always, and with each down not getting you more than half way up the next up before you're under your own steam again. 

At this point Dad and Dharms lack of specific training, and Dharms bad knees did start to take their toll. I stayed solidly up the front here until we got to the second info control! What time is the Saturday collection from the postbox at 55km? Well, 07:00 of course!


![A red postbox in a rough brick wall. Collection time 9:00am weekdays, 7:00am Saturdays](photo_2025-02-16_18-16-00-postbox.jpg "A red postbox in a rough brick wall. Collection time 9:00am weekdays, 7:00am Saturdays")

The next part was definitely harder than before, as we twisted and wound our way around the lanes, up and down, beautiful scenery if you could lift your head up over the hedgerows. It was still bitterly cold, the sun had come out a bit between the first full stop and this info control but now it was solidly hiding away. The pace slowed, and I stayed at the front, only rolling away on one major hill for leak #5... 

I don't remember much, other than the Harrington(worth?) viaduct, apparently the longest viaduct in the world when it was built and maybe possibly the longest in the UK still (not checking that fact, and my Dad wasn't 100% on the details, but it's a very nice viaduct). We past along the hill I'd had a puncture on the year before, stopped at the top of the hill for a snack break in the same place I'd mended my puncture, outside the caravan place. 

When we got to the 2nd full stop at East Carlton country park we were surprised that Lou and Nick had only just beaten us there by a few minutes. Our steady, non stop if slow, efforts had done us well. We were comfortably at around 15kmh here but I can't remember exactly. We had around 1h15m until that stop closed.

The lovely Phil was there again, checking us off, losing his paperwork every 5 minutes. It was at this point, not long after getting our stamps, that Dharms asked about a quicker way home, his knee was bad. It's just unfortunate that there isn't one. A few different ways back to the scout hut we'd started at, but all of them roughly 28km or so, rolling hills but generally downhill (now, that's not to say it's all downhill or that there are no hills, but there's 300m of elevation to lose, so it must be downhill!)

Fortunately Phil was able to give Dharms a lift back, he was struggling, and it's best to stop before you're out for a long time. Hopefully he can get back to his best, but still it was a good ride. 72km in, and the hardest of those done. 

I managed to pat a really fluffy dog, who loved me because my hands tasted like sausage rolls. Lou and Nick left ahead of us, as they were already going along. My Dad was struggling at this point too, blaming the cold, but also (correctly) severe lack of prep and training. Whelp, I'd set out to be a windblock, so a windblock I'd be all the way back. 

### Homeward Bound
Off we go, heading home. A wind block I'd set out to be, and I windblock I would be for the journey home. My favourite part of the ride from the year before, though that was partially helped by it being warm at that point in the ride, and the relief of not having a continual puncture as I was worried about while waiting at Carlton. 

It was still bitterly cold, and we left the carpark with an average of 13.7kmh. Easily doable to get back, we'd have to lose a lot of time, our average moving speed should be above 12kmh. 

It was a rough ride back, but I did remember the route for the most part, even if Dads GPS packed it up, and mine started to lag a little more. There's not many turns on the final leg though, a 10km to next thing on my Garmin being the only time I've seen that widget be double digits. 

The best part of this section was an almost cyclocross part, so much mud had been brought onto the road by some tractors and stuff off the fields. Both me and Dad are on CX bikes with mudguards, but I could hear Lou and Nick cursing me, their mudguardless asses, and 25mm tyres (they get me back on the fast road sections in summer, unless I can get onto a road road bike for longer rides)

It was a decent ride, but again nothing to report for this next section. Finally Dad had to ditch the big ring, he'd ridden the whole way so far in the 46t and not even with the use of his 34t rear, due to not telling me it wasn't getting into that cog (I'd have fixed it if he'd said, but he never did). It was a 15% section he had to give up, and I'm glad he then did for the rest of the ride, definitely made it easier on him. 

The final 5km is pretty much entirely downhill, and with a tailwind and planes to look at it's great. It still dragged, and at this point I did just want the ride to be over. So close, but so far, the smell of curry at the scout hut almost in my nostrils. 

### Arrive
We got back to the hall, with only a few still there, and not many more to come, but with a healthy amount of time left on the clock. Nick and Lou had ridden off, and Dharms was safely back with Phil and headed off as we got inside. All done and safe. 

Cards stamped, cake to be eaten, and a word as we'd come in that possibly a friend who was supposed to be with us had turned up just late. He'd showed up to the final stop with around 20 minutes to that closure (Dharms texted as he was waiting with Phil until the timecut). 

Another 1 point Brevet in the bag, Dad decidedly not getting back on a bike ever again, and certainly not doing this next year. 



![A card with 'Rutland and Beyond' and 100km audax stamp on the first third, then 4 boxes for first the start, then the first info answer of blue, then a stamped smiley face for the first cafe stop, the second info answer of 07:00, and finally across on the 3rd segment the two stamps for East Carltons second cafe stop, and the finish at 16:02.](photo_2025-02-16_18-16-00-brevet1.jpg "A card with 'Rutland and Beyond' and 100km audax stamp on the first third, then 4 boxes for first the start, then the first info answer of blue, then a stamped smiley face for the first cafe stop, the second info answer of 07:00, and finally across on the 3rd segment the two stamps for East Carltons second cafe stop, and the finish at 16:02.")

![The back of the brevet card with the Audax Certified stamp upside down, and 7h32m time taken](photo_2025-02-16_18-16-00-brevet2.jpg "The back of the brevet card with the Audax Certified stamp upside down, and 7h32m time taken")

### Home, and Beyond
We left the scout hut finish for our final ride home, accepting we wouldn't see Simon come in (though waiting for lights we saw coming down the hill to check it wasn't him). Up the hill was a rough one, it's a tough start straight out of the scout hut up a 12% grade, but after the rest of the ride it's brutal. Dad had to walk a bit after a bad gear change on a restart half way up (never stop on a hill). 

It was a bitter ride home, I could definitely tell he hadn't entirely enjoyed the day out, but enough of that old curmudgen! 

I enjoyed the ride a lot, and will be back next year. It's a great challenge for me at the start of the year, forces me to do longer rides and make sure I'm in good shape for spring, but not so hard that I have to focus only on longer road rides. I can still smash around on the MTB if the weather permits. Next year I might be on my own, or I'll join an adhock group, because it seems nobody else wants to come back that I tricked into coming >:3c 

But that's it from this wonderful audax, and the longest post I've so far written. Here's to some more rides, and next stop sub 4h 100km time trial (not an official mark, but something I can definitely do with the right setup and planning, but not something I can just do on a whim or have ever manged before)

![The same bike from the start, in dusk, facing in towards the shed much muddier having now finished the ride](photo_2025-02-16_18-16-01.jpg "The same bike from the start, in dusk, facing in towards the shed much muddier having now finished the ride")

![Strava stats page for the ride. 128.07km. 7h18m56s moving time. 1,459m elevation, 93w Est power average (ignore that, it's rubbish), 2,456kJ energy output. Speed: 17.5kmh avg, 47.7kmh max. Calories: 3,448. Temp: 0C. Elapsed time: 9h54m03s](photo_2025-02-16_18-11-15.jpg "Strava stats page for the ride. 128.07km. 7h18m56s moving time. 1,459m elevation, 93w Est power average (ignore that, it's rubbish), 2,456kJ energy output. Speed: 17.5kmh avg, 47.7kmh max. Calories: 3,448. Temp: 0C. Elapsed time: 9h54m03s")