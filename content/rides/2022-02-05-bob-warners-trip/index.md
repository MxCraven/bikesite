+++
title = "Old Bike Shops, and 'Bob'"
date = 2022-02-05T16:39:41Z
[taxonomies]
tags = ["rides", "bike-commuting", "text"]
+++

In my city there's an old bike shop, it's a wonderous place full of imagination, and a bike mechanical master as old as time. The shop it called "Bob Warner Cycles", but the mechanic is called John. 

I went there for some parts for [Project Postie](/tags/project-postie). A few bearings, a bit of knowledge, and to allow myself to buy everything else online having physically been to a bike shop (a proper one) to settle my concious. 

This isn't about the parts. This is about the ride. I, of course, chose my trusty bicycle. I don't have the postie, which is my cargo bike, so took my rapid day to day commuter. Google says 35 minutes, I thought I could beat that, assuming I didn't get lost (I got lost). I rode out, following the little lady in my phone yelling directions at me, and travelled right into a block headwind until I got to the "centre" of the city. Then I got lost a few times, couldn't follow the satnav, and eventually found myself confused by it making me pull a huge U turn, but eventually I did find the place. 

After exiting with my parts (that cost a whole £4 for 2 caged bearings, 2 cotterpins, and a pack of loose 3/16ths... Because this man hasn't updated his prices since decimalised currency...) I was putting my helmet and gloves on as a random older man popped over to ask about my saddle cover. Unfortunately, it's nothing special. A weird ebay silver bag that's probably spray painted silver because it rubs off really fast (useful if you're not sure where you actually chafe on the bike). I only had my advice of using a saddle cover backwards. Put it on when you ride, and take it off when you stop. This means you've always got a wet saddle, but the thing you sit on is always dry! 

Coming home after was much easier, as I was sent past my work (the way I'll take in future when I go to Bob's) and with the tailwind I flew home. 

After I arrived I attempted to fit the bearings, and realised I had the wrong sized cages. John had said I could return them, but for £1, and the fact it was my fault for not having a comparative thing, it's really not worth it... I just bought another set online, which will arrive with all the postie stuff, and for now will have to endure a really really crackly headset on my gravel bike. 

Anyway, thought I'd post because I need to more. This is the first post expressly written for the new site style (now on Zola), and hopefully it's all working alright. 
