 
+++
title = "Please turn right, into this anti-bike gate! (and some puncture stuff)"
date = 2022-05-01T00:00:00
[taxonomies]
tags = ["rides", "punctures", "bugger"]
+++

I went on a ride after work on Friday, I took my MTB down to the track! It was going well, some lads were there (a bit dumb and young and annoying, but well meaning enough at least). It was great fun, they were riding well and asking me to help them jump further and giving me the motivation to try new tricks (like no footers, which are easier than one footers I think. Still crap at them)

Just as I was about to leave, my rear tyre started going down... Well, I had some mucoff slime in there, so I rode about a bit and hoped it'd seal... It didn't. So I took my tools out, and set about removing the wheel. Except... Fuck. No 5mm, and I installed allen key skewers instead of QR ones (for anti branch death protection, and steeze factor). Ah well, I have patches in my pack as well as a tube, so I put one on. 

It fixed it, but I thought it was still going down, so I tried pumping and pumping aaaand... My pump broke... Fuck fuck fuck. So I had to walk home. That *really* sucked. A 15 minute easy ride turned into a 45 minute walk/jog. 

Today I headed out again, having "mended" the holes. I'd found 2 thorns and removed them, and thought I was in the clear... Well, after my floor pump also died... That fuckin sucked. Need to buy a new one of those, because my "new" pump doesn't work on the presta end for some reason. 

No. Half way to the track I had to admit it was definitely going down. I carefully inspected everything and there was a 3rd fucking thorn in my tyre! I've got no idea if the 2nd and 3rd thorn were from walking home, or if I went through 3 thorns, or if they'd got in there at a previous point but never punctured the tyre and walking home with a flat tyre made them go through... That fucking sucked, but I was able to change it. It wasn't such a bad place to stop anyway: 
![](IMG_20220501_132926.jpg "My bike, upside down with the wheel out. The wheel is lent up against a large rock (there's lots of them in a circle). My toolkit is on top of the rock. It's by some trees and a large open water lake. A very nice place to stop to mend a puncture, especially compared to the usual wherever by the side of the road")

On my way into the park on Friday I took note of this sign: 
![](IMG_20220501_131644.jpg "My bike wedged between an anti-bike gate. The sign above says 'Please turn right, no through route for cyclists'. It turns you into Route 48, which is a shared bike and walking route, and away from the always empty canalside path that leads to the same place (literally the other side of some bushes)")
Today I took a picture of it. It's only recently that I've come down this way, and only because of the canal path really because there's also a road route into the park (for cars, who pay exorbitant amounts to park and then people get bikes out and cycle around... Madness)

This sign annoys me, because the canals were built for horses but slowly the horse and bike allowed status of them is being removed by the canals trust. Now, those people do good work making these things safe, but it is *very* annoying that this local park to me loves to remove cycling side paths as much as possible, because "cyclists too fast" but then force those same fast cyclists onto the same paths as walkers and random ass dogs who like to cut you off. 

I cycle slowly through the park anyway (most of the time) but even then people just won't move out of the way when you ding a bell, or dogs will cut straight across. It's a pain in the arse. 

This sign saying "Please Turn Right, No through route for cyclists" is a shining example of how in the UK cyclists are only remembered so our ability to get around can be cut. 

Anyway, we'll end on a nice part. This ride was really good in the end, and I'm glad I went on it. Despite the punctures, despite the crappy signage and bike routes sucking, and despite annoying kids it was an enjoyable ride. I was tired before I went out, and I'm less tired now. 

Definitely need to finish the postie though, because that's the correct bike to take out for an easy Sunday rest ride. 