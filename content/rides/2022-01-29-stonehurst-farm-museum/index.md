+++
title = "Stonehurst Farm Museum"
date = 2022-01-29T16:43:41Z
[taxonomies]
tags = ["rides", "text"]
+++


On todays Saturday morning group ride, we took a gentle pedal with the shortest group as there weren't enough riders for the medium distance. It's always enjoyable to ride with that group, as there are knowledgable and interesting riders like Tim, who is 68 (nearly 69) and rode in his youth before picking it back up during 2020.   
  
I enjoy the steady pace of these rides. I can sit happily on the front, barely put any effort in, and pull the group through strong winds (like we had today.) Today I was faffing around with my seat. I've recently learned that a wrongly set saddle can leave you leaning over towards one side of the bike, and I can always see my right hand rim wall of my bike, so I assumed something was up with the saddle. It turns out my levers are just twisted the wrong way (something easy to solve, without replacing bar tape)  
  
The route today took us (after much change and deliberation) to Stonehurst Farm. This farm had a small museum of old cars, motorbikes, and most important to me, it had bikes hanging from the ceiling! I generally just enjoyed walking through and looking at these while waiting for the cake and hot chocolate I ordered to arrive. It was lovely. Looking at the bikes, wondering what they were used for, how they were ridden, seeing what wear was on the bike. The red drop bar bike (bike 1) had reasonably worn bar tape and tyres showing it's been well loved and ridden. The sit up black bike (bike 2) seems to have been restored. It's got almost new tyres, is very clean and has no rust or oil anywhere, and a beautiful Sturmey Archer Dynohub in the rear! 

The 3rd and 4th bikes were the same, but with the 4th in much worse condition. A completely rotted rear tyre (solid, no tube) and a lot more rust (which I was unable to get on the camera due to lights in the barn)  
  
I did like this place. It's nice to go somewhere that has a nice feel to it, and just something to look at. Ignoring that there were a lot of pro car adverts of course. Those commute, runaround style bikes stand in defiance, and the simple roadster would be a beautiful steed in any time period. 

Oh yeah, and there was a "Craven A" cigarette sign, which I took a photo under, but won't show because I look like a twat in ill fitting lycra (this is only helped on the bike by the fact I can ride quite quick and anyone who's fitter than me almost certainly has a faster, more sleek looking bike, so I can use that excuse :P)

Bike 1:
![](IMG_20220129_105031.jpg)

Bike 2:
![](IMG_20220129_105040.jpg)

Bike 3:
![](IMG_20220129_105050.jpg)

Bike 4:
![](IMG_20220129_105058.jpg)
