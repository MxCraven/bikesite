+++
title="Cafe Rides 002: Borrough Court"
date=2025-01-25

[taxonomies]
tags=["rides", "cafe-rides", "tdq"]
+++

### The Ride

Late start to this one, at 11:00. I thought it'd be cancelled with an ice warning only ending at 10:00, but the message went out it was going ahead, though there were groups going off at various times. 

Our 11:00 starters decided to keep it short, and head a simple 20ish miles to Borrough Court. We set off gently into the wind and headed towards Barkby, picking up a friend at the top of the hill in Croxton, then splitting in Foalville for those who wanted a cafe and those who needed home. 

Up Borrough Hill to the cafe (although only half way) it got quite warm, still glad I took my larger coat though. A nice sun and gentle climb is a good day for me :3 

### The Cafe

Cafe is nice, I've not been before. Good selection, and the hot chocolate was amazing! It had chocolate whipped cream on top! Big cups too. I had a lovely thicc sausage roll, and various bacon and sausage cobs were spread around elsewhere. 

The cafe is small inside, but a big outside so it'll be good in the summer. It was only us inside and it was nice and warm, so great on that one. 

### Homeward Bound
Heading home we just dropped down the hill and back the way we came, detouring at Barsby to head back through Gaddesby and home. A good ride in the conditions, kept at a low speed and just chilled out how most easy winter rides should be! 

Planning onwards for bigger rides in the summer was the chat today, whether Hunstanton is the call again this year, or instead heading around Leicestershire for the 100. Next up though, Rutland and Beyond in 3 weekends time! Close close close! 