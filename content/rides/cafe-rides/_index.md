+++
title = "Cafe Rides"
transparent = true
paginate_by = 10
sort_by = "date"
template = "section.html"
+++


### Cafe Map

<img src="CafeMap.jpg" alt="A map of a section of Leicestershire with dots in the places of cafes I've been to (very rough)" style="width:100%;">
<small> 3 cafes </small>
