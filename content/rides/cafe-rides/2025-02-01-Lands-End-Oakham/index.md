+++
title="Cafe Rides 003: Lands End Oakham"
date=2025-02-01

[taxonomies]
tags=["rides", "cafe-rides", "tdq"]
+++

### The Ride

The first regular TdQ ride of the year! Finally starting at 0900, with everyone, and no reason to delay! It was a bit drab and drizzly though. The ride out to the foot of Borrough Hill was gentle, but when we got to Twyford it split heavily between different people going different directions, and some getting lost. My route was led out by Greg down the muddier back lanes that make up the feedzone and a few laps of the Melton Oakham (none of the true off road bits, but the Sommerberg is getting "resurfaced" before the womens race in a few weeks). 

Everyone got to the cafe alright, and it was fun to get a bit of speed up early in the year, though I'm tonining it down to let myself get used to gentler riding for the Audax in 2 weeks. 

### The Cafe

This cafe has been a regular cold day haunt for us since it opened last year (or the year before?). It's big inside, quick service, plenty of options, and warm. A lot of people opted for the "build your own" style breakfast (bacon, toast, eggy bread, beans, it's all available) but I went for a toastie (I'll usually go for a paninis but they didn't have any in a flavour that took my fancy). 

The favoured part here for some is that the coffee is just a simple machine, nothing fancy, it just works. The hot chocolate is strong, and thick, though you do have to check you select "chocolate" and not "chocolatte" (choco latte, coffee)... Fortunately my eyes still work well enough to not make that mistake, but one day I'll need a set of glasses for cycling with (or maybe I'll get a stem cap mounted monacle)

### Homeward Bound
All together for the way home, we thought. Then Phil had a slow puncture. I offered a pump, but had stupidly forgotten it on the fridge after I'd replaced my chain and cassette a few days before (I didn't think it was interesting enough to write about). I borrowed one from Nick and got to reversing the slow leak, while Phil had a quick one :P.

The rest of the group said they'd ride slow so we could catch up, and we went fast since the puncture was still going down to get home before it was gone. The blast back down through Gaddesby felt amazing, nowhere near my best (over 1 minute slower) but in winter kit, and having smashed that section on a TT set up road bike (more on that soon) I thought averaging 20mph wasn't too bad. Again, time to slow down. 

A good ride, and a good cafe all around. Hopefully we're riding every week for a good length now! 