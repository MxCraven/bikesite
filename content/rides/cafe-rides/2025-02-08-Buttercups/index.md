+++
title = "Cafe Rides 004: Buttercups Cafe"
date = 2025-02-08

[taxonomies]
tags = ["rides", "cafe-rides", "tdq"]
+++

### The Ride Out
We weren't sure if this ride was going ahead this week, the rain was forecast as "likely but light" and it was on the edge of cold. Of course I jinxed it with [last weeks post](/rides/cafe-rides/lands-end-oakham)! Still, I decided to go because I needed miles anyway, if anyone else turned up I'd go with them. 

<img src="lou-mech.png" alt="A group of cyclists stood around as one faffs with his bike" width="50%" height="50%">

It was cold, but the pace on the way out was fairly easy, Nick rolled off the front by accident a few times, but we rolled gently over. Lou managed to break his AXS battery somehow, despite showing fully charged it was dead, so he was stopping before hills to put the back in the front, change the front, and get riding again! 

There were a few hills, but they were good ones, like Tilton! I'm feeling decent on steep hills still, but not so snappy as I'd like to be. We'll see if I work on that, or need to keep more power towards the long efforts for TTs. 

### The Cafe Stop
Buttercups is always lovely, good food, warm and large inside, and it's quick. Mostly warm going around this week, though I missed Dad saying he'd like a cake and got sausage sammich instead for us. I'd thought about the orange cocolate cake too, but oh well, good sausages! 

This cafe has some animals outside, today in the front were some birds (like geese, but I'm not sure) and the alpaccas were around the back, looking cold. 

### Homewards Bound
It fogged over and misted up as we got ready to head home, and it was definitely cold coming out of the cafe. I'm glad I took my jacket off while I was inside so I didn't get used to the extra warmth. 

Riding home was mostly downhill with a few valley dive and backups, Amanda did a great ride getting up hills fast so hopefully she sees them in a better light soon (doubt it, they never get any easier, you just get faster!) The fog cleared, and with a few extra pushes of speed we got home mostly warm. 

All in, a good ride. Just one more week until the Rutland and Beyond Audax, so one ride Saturday and then we're onto Sunday for 125km, early starts, and brevet cards. It's looking like it'll be a cold one though. 

<img src="group-pic.png" alt="A poicture of a group of cyclists, one taking the selfie in orange, 3 yellow, 2 black, 1 blue. It's cold, we're smiling outwardly." width="50%" height="50%">