+++
title="Cafe Rides 001: Eye Kettleby Lakes"
date=2025-01-18

[taxonomies]
tags=["rides", "cafe-rides", "tdq"]
+++

A start to me hopefully remembering to vaguely review cafes we go to over the year! First off, a chilly visit to Eye Kettleby Lakes. 

The ride was a fairly brisk pace, ended up at just over 50km, but easily doable with 5 of us sharing the pacemaking. Borrough Hill as always thoroughly enjoyed. Initially we were going to head to the Dairy Barn, but it was freezing as we passed and we agreed to just continue further down the ride a few km and stop at Eye Kettleby. 

It's a good place to go when you're cold, though we could have done with getting closer to the fire! A minus 3C throughout definitely needed a hot snack at the cafe. The 5 of us that were there between us got 4 coffees, and 1 deluxe hot chocolate (hi, that's always me). The waitress looked at me directly and said "I'm not sure who's ordered the hot chocolate" and yes, hi, that'd be me. 

The food was quick, with one brownie arriving quickly, a sausage and egg cob, two toast and egg (one scrambled, one fried), and a sausage cob for me. The sausage cob was great, came with 3 whole sausages (cut in halfs) and I had to squash it down to fit it in my gob! Brown sauce supplied, always good. 

Overall, definitely good :3 Warm, easy, and a decent hill to climb to get in which is always a bonus! 