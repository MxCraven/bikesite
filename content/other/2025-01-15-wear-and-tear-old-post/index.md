+++
title = "Wearing and Tearing (an old post)"
date = 2025-01-15
[taxonomies]
tags = ["other", "ramble"]
+++
 
This post was originally written for my first SSG website, 2019-04-28. I'm posting it here so I can forget about it really. 
If you've seen this in your RSS feed, hi Dee and Steph! I'm gonna try remembering to post now

I have an odd obsession with general wear and tear. I love the idea of wearing out thing like tyres and keyboards. It's an odd thing I've always liked. I can't remember why honestly.
When I played F1 games as a kid I would put no track limit, no other cars, and just drive, but I would have wear and tear on! It was fun to just wear the track out.
I like off road racing too and seeing the grass get turned to mud in the racing line. That's really cool.
I always look at things carefully to see their wear when I've just got them, but after that I often forget about them. I forgot about my laptop for the past year and I've just looked at it again, and despite it being hooked up to an external keyboard most of the time, it's getting very worn. I love that.
Part of it I think comes from my school, and specifically the local vicar. We got Bibles when we were in a certain year (I think year 4?) and she said that the next time she saw us she wanted to see the books pages grubby and the corners folded. That showed it had been used! I think I read that Bible once, but the whole idea of using something and making it look worn has stuck with me.
My sister reads a lot and she always likes to make her books look like they've never been read. I ride bikes, and if there wasn't a scratch on my bikes I'd say I haven't ridden them enough! When I got a fingerboard ramp I was annoyed that it looked so new, so I constantly rode that ledge again and again and again until it looked well used, but the thing is it was well used. I must have done 2-3 thousand tricks in a few days! I don't wear things out on purpose, and I still do (mostly) take care of my kit!
The laptop I'm writing this on is something I hope I'll still be using for quite a while. I got it as a laptop that I can take anywhere I go and can do basically anything I need. It's a Thinkpad (x230) so I know the keyboard is really good and strong. I'm starting to see just a little bit of wear on the spacebar because I kinda swipe on my spacebar when I press it (typing properly with my left hand and just with one finger on my right!) This shows up really well at work where I'm usually wearing gloves. Even though we don't type a lot, the space bar is worn in one year as much as my gaming laptop in over 2! I've definitely used my laptop more!
I think that's what makes me enjoy the really boring task of polishing stone at work, and also cutting samples. We sell stone floor tiles, and for samples we cut lots of smaller tiles from the big ones. It's repetetive and boring, but I like it because it's wear and tear and it's growing large numbers of things.
Well, there's a little insight into my mind and my love of wear and tear. Maybe I'll think of something else I want to talk about and write it up in this way :3
