+++
title = "Food: ADHD Flapjack Recipie for Bike Energies"
date = 2022-04-13T18:30:00
[taxonomies]
tags = ["other", "recipies"]
+++

Ingredients: 
Porridge Oats/Jumbo Oats (200g/250g if no fruit)
Brown sugar (but any kinda works, just use less if it's white) (50-100g depends on taste)
Butter (no idea if salted or unsalted, I use either) (125g) 
Golden syrup (2tbsp? IDK, I use the measurement of "some")
Fruit additives, or chocolate, or basically anything you like. Nothing too wet.

Hi there! I make flapjacks as bike food, but also packed lunch snacks for work. I found a few recipies on tinterweb and did some shit to them to make them easy to make! 

So here goes some instructions for dumbass flapjacks that taste pretty good. 

![](IMG_20220411_194235.jpg "200g oats in a big mixing bowl")
![](IMG_20220411_194343.jpg "70gish of sugar in the bowl")
![](IMG_20220411_194457.jpg "a layering of cranberries and rasnins in the bowl too")

Chuck the oats, sugar, and mixings into a bowl. If it's chocolate you might want to melt it in with the butter later, unless you want chunks, but it still will probably melt in a bit. Amounts are honestly incredibly random. Every batch is different and the point of this is kinda guessing until you figure what you want out. Mix that all together

![](IMG_20220411_194941.jpg "The dry stuff mixed together in the big bowl")
![](IMG_20220411_194553.jpg "cutting a 250g block of butter in half")
![](IMG_20220411_194939.jpg "Melted butter in a bowl")


After that get your butter and melt it in the microwave (or saucepan, if you don't have a microwave). Doesn't need to be perfectly melted, just enough to mix in with the dry shit. 

![](IMG_20220411_195047.jpg "Mixed in butter")

Then add the golden syrup to an amount of "some". I forgot to take a picture of this, but it's honestly guesswork, especially since I use a squeezy bottle of the stuff so I can't even measure it if I try. I prefer the sqeezey because you don't lose half of it stuck to a spoon every time. Make them a few times and you'll figure it out. More is better here though. This foodstuff definitely isn't healthy, but it is tasty and for energy bar alternatives it's great. If you don't use enough don't worry, it'll just be a granola. 

When mixing the golden syrup you might need to microwave the whole bowl to make it easier to work in. Depends on ambient temps. 

![](IMG_20220411_195325.jpg "A reusable baking paper over a tin")

Put some baking paper into a tin, I use a disposable BBQ one because it's the right kinda size. This is meant to be for a 20cm x 20cm tin though, so whatever's closest to that. Then pour your mix in, and push it all around until it's smooth. This is where you can usually feel if it's too dry (this mix was just a bit dry, but works and holds together fine). If you want to add more of anything you can always throw it back in the bowl at this stage. 

![](IMG_20220411_195429.jpg "The mix pushed into the tin")

Next throw it in the oven. This is where we get into *very* inexact science. I put mine in at 180*C (fan oven) for 20 minutes, NOT preheating the oven. That's the basically perfect time for me. What you want is to see the mix just about bubbling. This means the sugar has melted a bit and gone into places to harden up after it cools. 

![](IMG_20220411_195456.jpg "An oven set to 180C")

Then once done, leave to cool and then cut into squares or circles or pentagrams and enjoy. I've got this down to 10 minutes prep time, and then just watch shit on the Internet while it's in the oven. It works well doing it once a week (usually) and I can get a weeks worth of packed lunches out of 1 batch, plus some snacks, plus some for the weekend rides :3 

Hopefully this was vaguely useful and made sense, IDK. 