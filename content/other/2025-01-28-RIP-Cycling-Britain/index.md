+++
title = "RIP To Cycle Sport in Britain"
date = 2025-01-28

[taxonomies]
tags = ["other", "ramble", "fuck"]
+++

![An image which shows the cricketting Ashes obituary, remade to say "In Affectionate Rememberance of UK CYCLING COVERAGE which died at the hands of WBD{WarnerBrosDiscovery} on 28th January 2025. Deeply lamented by a large circle of friends and aquaintances. R.I.P. N.B-The body will be cremated and the ashes taken to TNTSports](rip-uk-cycling.png  "An image which shows the cricketting Ashes obituary, remade to say 'In Affectionate Rememberance of UK CYCLING COVERAGE which died at the hands of WBD{WarnerBrosDiscovery} on 28th January 2025. Deeply lamented by a large circle of friends and aquaintances. R.I.P. N.B-The body will be cremated and the ashes taken to TNTSports'")

---

This post copied from a reddit comment I made in reply to Eurosport no longer existing in the UK, and the general death of the sport of cycling professionally in the UK in the past few years...

RIP to the sport of cycling in the UK. We're losing ITV4 coverage of the Tour, British Cycling didn't cover Nationals Cyclocross at all and they don't exactly push the National Road Series.

When I first got into cycling as a little kid I watched and rewatched DVDs of Eddy Merckx, and Armstrong, stories of the 60s and 50s at the Tour, and I must have watched the 2006 Tour of Britain 50 times over because there was no fresh coverage of anything.

Back when the best British hopeful was Maggy Backstead (he lived in Wales, that counts right?) because he won Roubaix, and it's a core memory for me having a dodgy recording of the last hour of Roubaix when Roger Hammond came 3rd (I'd met him that year at a random cyclocross race locally).

Then along comes Cav, and G, and Froome, and Wiggins, and of course the amazing 2008-2016 track racing at the Olympics, it was looking up (though at that time I missed the boat to go pro and get on it). British Cycling touted all of these riders as "theirs", though I suspect they had a lot less to do with the success than they like to claim (Cav never made their strict guidelines, but you can't stop a bloke who's winning going to worlds).

The worst story is them not wanting to give a speedsuit to the womens team in 2008 worlds, because the mens team was unlikely to win so they never made them. I can't remember which team mate it was, but her and Nicole Cooke sewed on the new sponsors to a suit from the year before, and at the end of the race Cooke was world and Olympic champion in the same year.

I had a British Cycling window sticker in my window for 15 years, I burned it last year in protest of their bullshit. It hurt. I wear a beanie from nearly 20 years ago, because it's the best damn winter under helmet hat I've got (they stopped making them shortly after anyway) and I feel bad when people see me in it, possibly thinking I support their killing of my beloved sport.

The sport of cycling in Britian will survive without BC, because clubs will keep it alive. It'll go back to where nobody is pro, and it'll struggle hard because of local bike shops no longer being able to "sponsor" clubs and riders for any more than grease and a service every few months. But it'll survive because people love it, and when the next wave of talent comes through I'm sure British Cycling will be there to sweep it up, and they'll fight to get it televised again and take the credit.

Anyway, I'm just mad the sport I love is dying...
