+++
title = "Creating a Group at Birstall Bike Track"
date = 2022-04-13T18:30:00
[taxonomies]
tags = ["other", "group-rides", "ramble"]
+++
 
 
What isn't a ramble around here, eh?

Anyway, I wanted to create some more activity at the bike track I go to, so decided to try to get people to meet at the same time each week. I put up some posters: 

![](IMG_20220408_165625.jpg "A sign with some information about the group, and a QR for a link to the website pinned to a gate at the park")

While I was there I saw a rider I've seen a few times, and confused him with what it was, but I *think* he got the idea? Maybe? Who knows... I'm sure he'll figure it out with the signs. 

Anyway, that QR code (and a direct link, because nobody uses QR codes but they basically just denote links these days) goes to [this site](http://birstall.magnoren.uk/). 

The plan, hopefully, is to create something where people turn up regularly and ride on a Friday after school and work, and we just kinda vibe I guess? It might die, I might not be able to run it due to not having the awful Facebook, but like... I can try, right? And at the very least I can be there every Friday. 

So I'm gonna try it. We'll see in a few days if anyone was intrested, or even if the signs are still there. I'm hopeful, and the ride out crew are going again on Sunday so I'm hoping to meet them there and invite anyone who cares along. We'll see how it all goes. 

This all feels quite pointless across the Internet really... If anyone can get to Birstall bike track (it's on Google Maps and such, or message me and I can send details) then hit me up, I'd love to ride with you. 